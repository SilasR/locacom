![](https://projets.silasriacourt.fr/S2/logo.png)

# LocaCom
Une agence est dans le besoin d informatiser sa gestion de panneaux publicitaires d une ville.

Le projet sur lequel nous travaillons consiste à gérer les locations de panneaux publicitaires. Afin de simplifier le rapport, nous avons décidé de nommer le projet LocaCom. L'équipe du projet est constituée des membres suivants :

- RIACOURT Silas
- LE BEC Owen
- BEAUCHARD Evann
- FIFRE Hugo
- ROPERT Fabien

Tuteur de projet :

- MARTIN Arnaud
