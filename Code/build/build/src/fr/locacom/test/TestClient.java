/**
 * 
 */
package fr.locacom.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import fr.locacom.model.Client;
import fr.locacom.model.ListeClients;
import javafx.embed.swing.JFXPanel;

/**
 * Cette classe permet de tester l'ajout 
 * et la suppression d'un client
 */
public class TestClient {

	/**
	 * Initialisation de JavaFX pour les tests
	 */
	@Before
	public void setUp() throws Exception {
		new JFXPanel();
	}
	
	/**
	 * Ce test vérifie l'ajout d'un client
	 */
	@Test
	public void ajoutClient() {
		//On vide la liste de clients
		ListeClients.getListeClients().clear();
		//On crée des éléments de test
		Client client = new Client("Fabien", "Lannion");
		//On vérifie que le client a été ajoutée à la liste
		assertEquals(client,ListeClients.getListeClients().get(0));
	}
	
	/**
	 * Ce test vérifie la suppression d'un client
	 */
	@Test
	public void suppressionClient() {
		//On vide la liste de clients
		ListeClients.getListeClients().clear();
		//On crée des éléments de test
		Client client = new Client("Fabien", "Lannion");
		//On ajoute le client à la liste
		ListeClients.getListeClients().add(client);
		//On supprime le client de la liste
		ListeClients.supprimerClient(client.getId());
		//On vérifie que la liste est vide
		assertTrue(ListeClients.getListeClients().isEmpty());
	}

}
