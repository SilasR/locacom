/**
 * 
 */
package fr.locacom.test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import fr.locacom.model.Client;
import fr.locacom.model.Date;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.Location;
import fr.locacom.model.Panneau;
import fr.locacom.model.Visibilite;
import javafx.embed.swing.JFXPanel;

/**
 * Cette classe permet de tester l'ajout 
 * et la suppression d'une location
 */
public class TestLocation {

	/**
	 * Initialisation de JavaFX pour les tests
	 */
	@Before
	public void setUp() throws Exception {
		new JFXPanel();
	}

	/**
	 * Ce test vérifie l'ajout d'une location
	 */
	@Test
	public void ajoutLocation() {
		//On vide la liste de locations
		ListeLocations.getListeLocations().clear();
		//On crée des éléments de test
		Client client = new Client("Fabien", "Lannion");
		Visibilite v = new Visibilite(0);
		Panneau panneau = new Panneau(1.2, v, 2, 4, "aaaa");
		Location location = new Location(5, client, panneau, new Date(5, 6, 1992), new Date(5, 6, 1992));
		//On ajoute la location à la liste
		ListeLocations.getListeLocations().add(location);
		//On vérifie que la location a été ajoutée à la liste
		assertEquals(location,ListeLocations.getListeLocations().get(0));
	}
	
	/**
	 * Ce test vérifie la suppression d'une location
	 */
	@Test
	public void suppressionLocation() {
		//On vide la liste de locations
		ListeLocations.getListeLocations().clear();
		//On crée des éléments de test
		Client client = new Client("Fabien", "Lannion");
		Visibilite v = new Visibilite(0);
		Panneau panneau = new Panneau(1.2, v, 2, 4, "aaaa");
		Location location = new Location(5, client, panneau, new Date(5, 6, 1992), new Date(5, 6, 1992));
		//On ajoute la location à la liste
		ListeLocations.getListeLocations().add(location);
		//On supprime la location de la liste
		ListeLocations.supprimerLocation(location.getIdLoca());
		//On vérifie que la liste est vide
		assertTrue(ListeLocations.getListeLocations().isEmpty());
	}

}
