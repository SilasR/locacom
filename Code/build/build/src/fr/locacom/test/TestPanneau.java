/**
 * 
 */
package fr.locacom.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import fr.locacom.model.ListePanneaux;
import fr.locacom.model.Panneau;
import fr.locacom.model.Visibilite;
import javafx.embed.swing.JFXPanel;

/**
 * Cette classe permet de tester l'ajout 
 * et la suppression d''un panneau
 */
public class TestPanneau {

	/**
	 * Initialisation de JavaFX pour les tests
	 */
	@Before
	public void setUp() throws Exception {
		new JFXPanel();
	}

	/**
	 * Ce test vérifie l'ajout d'un client
	 */
	@Test
	public void ajoutPanneau() {
		//On vide la liste de panneaux
		ListePanneaux.getListePanneaux().clear();
		//On crée des éléments de test
		Visibilite v = new Visibilite(0);
		Panneau panneau = new Panneau(1.2, v, 2, 4, "aaaa");
		//On ajoute le panneau à la liste
		ListePanneaux.getListePanneaux().add(panneau);
		//On vérifie que le panneau a été ajoutée à la liste
		assertEquals(panneau,ListePanneaux.getListePanneaux().get(0));
	}
	
	/**
	 * Ce test vérifie la suppression d'un panneau
	 */
	@Test
	public void suppressionPanneau() {
		//On vide la liste de panneaux
		ListePanneaux.getListePanneaux().clear();
		//On crée des éléments de test
		Visibilite v = new Visibilite(0);
		Panneau panneau = new Panneau(1.2, v, 2, 4, "aaaa");
		//On ajoute le panneau à la liste
		ListePanneaux.getListePanneaux().add(panneau);
		//On supprime le panneau de la liste
		ListePanneaux.supprimerPanneau(panneau.getId());
		//On vérifie que la liste est vide
		assertTrue(ListePanneaux.getListePanneaux().isEmpty());
	}

}
