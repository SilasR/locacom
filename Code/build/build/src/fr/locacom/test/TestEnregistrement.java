/**
 * 
 */
package fr.locacom.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr.locacom.model.ListeClients;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.ListePanneaux;


/**
 * Cette classe permet de tester l'enregistrement
 * des panneaux, des locations et des clients
 */
public class TestEnregistrement{

	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * Ce test vérifie l'enregistrement des clients
	 */
	@Test
	public void enregistrementClients() {
		//On vérifie que l'enregistrement de clients n'a généré aucune erreur
		assertTrue(ListeClients.enregistrerClients());
	}
	
	/**
	 * Ce test vérifie l'enregistrement des locations
	 */
	@Test
	public void enregistrementLocations() {
		//On vérifie que l'enregistrement de locations n'a généré aucune erreur
		assertTrue(ListeLocations.enregistrerLocations());
	}
	
	/**
	 * Ce test vérifie l'enregistrement des panneaux
	 */
	@Test
	public void enregistrementPanneaux() {
		//On vérifie que l'enregistrement de panneaux n'a généré aucune erreur
		assertTrue(ListePanneaux.enregistrerPanneaux());
	}

}
