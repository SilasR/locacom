package fr.locacom.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Classe permettant de gerer une liste de clients et son enregistrement dans un
 * fichier .xml
 */
public class ListeClients {

	/** ArrayList qui contient tous les clients */
	private static ArrayList<Client> ListeClients = new ArrayList<Client>();

	/**
	 * @param id
	 * @param nom
	 * @param coords
	 * 
	 *               Methode pour modifier un client grace a son id
	 */
	public static void modifierClient(int id, String nom, String coords,String telephone) {
		Client c = retourneClients(id);
		ListeClients.get(ListeClients.indexOf(c)).setCoord(coords);
		ListeClients.get(ListeClients.indexOf(c)).setNom(nom);
		ListeClients.get(ListeClients.indexOf(c)).setTelephone(telephone);;
		enregistrerClients();
	}

	/**
	 * @param c
	 * 
	 *          Méthode pour ajouter un client dans la liste
	 */
	public void ajoutClient(Client c) {
		ListeClients.add(c);
	}

	/**
	 * @param c
	 * 
	 *          Methode pour supprimer un client de la liste
	 */
	public static void supprimerClient(int id) {
		Iterator<Client> itr = ListeClients.iterator();
		while (itr.hasNext()) {
			Client c = itr.next();
			if (c.getId() == id) {
				itr.remove();
			}
		}

	}

	/**
	 * @param id
	 * @return {@link Client}
	 * 
	 *         Méthode pour retourner un Client de la liste grace a son ID
	 */
	public static Client retourneClients(int id) {
		Client client = null;
		for (Client c : ListeClients) {
			if (c.getId() == id) {
				client = c;
			}
		}
		return client;
	}

	/**
	 * Methode permettant d'enregistrer tous les clients dans un fichier XML
	 */
	public static boolean enregistrerClients() {
		System.out.println("j'enregistre les clients");
		boolean success = false;

		try {

			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();

			// cr�ation document
			final Document document = builder.newDocument();

			final Element racine = document.createElement("clients");
			// Sauvgarde du compteur d'id clients pour avoir un ID unique
			racine.setAttribute("nbCli", Integer.toString(Client.nbrcli));
			document.appendChild(racine);

			// Enregistrement de chaque client
			for (Client c : ListeClients) {

				final Element client = document.createElement("client");

				racine.appendChild(client);

				final Element id = document.createElement("id");
				id.appendChild(document.createTextNode(Integer.toString(c.getId())));

				final Element nom = document.createElement("nom");
				nom.appendChild(document.createTextNode(c.getNom()));

				final Element coords = document.createElement("coords");
				coords.appendChild(document.createTextNode(c.getCoord()));

				final Element resa = document.createElement("nbResa");
				resa.appendChild(document.createTextNode(Integer.toString(c.getNbResa())));
				
				final Element tel = document.createElement("telephone");
				tel.appendChild(document.createTextNode(c.getTelephone()));		
			
				client.appendChild(id);
				client.appendChild(nom);
				client.appendChild(coords);
				client.appendChild(resa);
				client.appendChild(tel);
			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			final DOMSource source = new DOMSource(document);
			final StreamResult sortie = new StreamResult(new File("clients.xml"));

			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			// formatage
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			transformer.transform(source, sortie);
			success = true;
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

		return success;
	}

	/**
	 * Methode permettant de recuperer les clients enregistrer dans un fichiers xml
	 * Recupere tous les clients et les remets dans la ListeClient
	 */
	public static boolean recupererClients() {
		System.out.println("Je recupère les clients");
		{
			boolean success = false;
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			File file = new File("clients.xml");
			if (file.exists()) {
				try {


					
					final DocumentBuilder builder = factory.newDocumentBuilder();
					final Document document = builder.parse(new File("clients.xml"));

					final Element racine = document.getDocumentElement();
					final NodeList racineNoeuds = racine.getChildNodes();
					final int nbRacineNoeuds = racineNoeuds.getLength();
					/*
					 * Permet de r�cup�rer les ids clients utilit�s reprends le compteur de client
					 * la ou il �tait
					 */
					Client.nbrcli = Integer.parseInt(racine.getAttribute("nbCli"));

					// On nettoie la liste avant de rajouter les clients
					ListeClients.clear();

					for (int i = 0; i < nbRacineNoeuds; i++) {
						if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
							final Element client = (Element) racineNoeuds.item(i);

							// Affichage d'un client

							final Element id = (Element) client.getElementsByTagName("id").item(0);
							final Element nom = (Element) client.getElementsByTagName("nom").item(0);
							final Element coords = (Element) client.getElementsByTagName("coords").item(0);
							final Element resa = (Element) client.getElementsByTagName("nbResa").item(0);
							final Element tel = (Element) client.getElementsByTagName("telephone").item(0);
							ListeClients.add(new Client(nom.getTextContent(), coords.getTextContent(),
									Integer.parseInt(resa.getTextContent()), Integer.parseInt(id.getTextContent()),tel.getTextContent()));

						}
					}
					success = true;
				} catch (final ParserConfigurationException e) {
					e.printStackTrace();
				} catch (final SAXException e) {
					e.printStackTrace();
				} catch (final IOException e) {
					e.printStackTrace();

				}
			} else {
				try {
					file.createNewFile();
					recupererClients();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return success;
		}
	}

	/**
	 * @return the listeClients
	 */
	public static ArrayList<Client> getListeClients() {
		return ListeClients;
	}

	/**
	 * @param listeClients the listeClients to set
	 */
	public static void setListeClients(ArrayList<Client> listeClients) {
		ListeClients = listeClients;
	}

}