package fr.locacom.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Classe permettant de gerer une liste de panneaux et son enregistrement dans
 * un fichier .xml
 */
public class ListePanneaux {

	/** ArrayList qui contient tous les panneaux */
	private static ArrayList<Panneau> ListePan = new ArrayList<Panneau>();

	/**
	 * @param p
	 * 
	 *          Methode pour ajouter un panneau dans la liste
	 */
	public void ajouterPan(Panneau p) {
		ListePan.add(p);
	}

	/**
	 * @param id
	 * 
	 *           Methode permettant de supprimer un panneau de la liste grace a son
	 *           ID
	 */
	public static void supprimerPanneau(int id) {
		Iterator<Panneau> itr = ListePan.iterator();
		while (itr.hasNext()) {
			Panneau p = itr.next();
			if (p.getId() == id) {
				itr.remove();
			}
		}

	}

	/**
	 * @param id
	 * @param details
	 * @param tarif
	 * @param longueur
	 * @param largeur
	 * @param visi
	 * 
	 *                 Methode pour modifier un panneau de la liste
	 */
	public static void modifierPanneau(int id, String details, Double tarif, int longueur, int largeur,
			Visibilite visi) {
		Panneau p = retournePanneau(id);
		ListePan.get(ListePan.indexOf(p)).setDetails(details);
		ListePan.get(ListePan.indexOf(p)).setTarif(tarif);
		ListePan.get(ListePan.indexOf(p)).setLargeur(largeur);
		ListePan.get(ListePan.indexOf(p)).setLongueur(longueur);
		ListePan.get(ListePan.indexOf(p)).setVisibilite(visi);

	}

	/**
	 * @param id
	 * @return
	 * 
	 *         Methode pour retourne le panneau avec l'ID passe en parametre
	 */
	public static Panneau retournePanneau(int id) {

		Panneau panneau = null;
		for (Panneau p : ListePan) {
			if (p.getId() == id) {
				panneau = p;
			}
		}
		return panneau;
	}

	/**
	 * Methode permettant d'enregistrer tous les panneaux dans un fichier XML
	 */
	public static boolean enregistrerPanneaux() {
		System.out.println("j'enregistre les panneaux");
		boolean success = false;
		try {

			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();

			// cr�ation document
			final Document document = builder.newDocument();

			final Element racine = document.createElement("panneaux");
			// Sauvgarde du compteur d'id clients pour avoir un ID unique
			racine.setAttribute("nbPan", Integer.toString(Panneau.nbrpan));
			document.appendChild(racine);

			// Enregistrement de chaque panneau
			for (Panneau p : ListePan) {

				final Element panneau = document.createElement("panneau");

				racine.appendChild(panneau);

				final Element id = document.createElement("id");
				id.appendChild(document.createTextNode(Integer.toString(p.getId())));

				final Element tarif = document.createElement("tarif");
				tarif.appendChild(document.createTextNode(Double.toString(p.getTarif())));

				final Element visibilite = document.createElement("visibilite");
				visibilite.appendChild(document.createTextNode(Integer.toString(p.getVisibilite().getId())));

				final Element longueur = document.createElement("longueur");
				longueur.appendChild(document.createTextNode(Integer.toString(p.getLongueur())));

				final Element largeur = document.createElement("largeur");
				largeur.appendChild(document.createTextNode(Integer.toString(p.getLargeur())));

				final Element details = document.createElement("details");
				details.appendChild(document.createTextNode(p.getDetails()));

				final Element locations = document.createElement("locations");

				/* pour toutes les locations du panneaux */
				for (Location l : ListeLocations.retourneLocation(p)) {
					final Element location = document.createElement("location");
					location.setAttribute("id", Integer.toString(l.getIdLoca()));
					locations.appendChild(location);
				}
				final Element coords = document.createElement("coords");
				final Element x = document.createElement("x");
				final Element y = document.createElement("y");
				x.appendChild(document.createTextNode(String.valueOf(p.getBouton().getLayoutX())));
				y.appendChild(document.createTextNode(String.valueOf(p.getBouton().getLayoutY())));
				coords.appendChild(x);
				coords.appendChild(y);
				panneau.appendChild(id);
				panneau.appendChild(tarif);
				panneau.appendChild(visibilite);
				panneau.appendChild(longueur);
				panneau.appendChild(largeur);
				panneau.appendChild(locations);
				panneau.appendChild(details);
				panneau.appendChild(coords);
				;
			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			final DOMSource source = new DOMSource(document);
			final StreamResult sortie = new StreamResult(new File("panneaux.xml"));

			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			// formatage
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			transformer.transform(source, sortie);
			success = true;
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

		return success;
	}

	/**
	 * Methode pour recuperer tous ls panneaux depuis le fichier XML
	 */
	public static boolean recupererPanneaux() {
		System.out.println("je recupere les panneaux");

		boolean success = false;
		{

			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			/* On regarde si le fichier éxiste */
			File file = new File("panneaux.xml");
			if (file.exists()) {

				try {

					final DocumentBuilder builder = factory.newDocumentBuilder();
					final Document document = builder.parse(new File("panneaux.xml"));

					final Element racine = document.getDocumentElement();
					final NodeList racineNoeuds = racine.getChildNodes();
					final int nbRacineNoeuds = racineNoeuds.getLength();
					/*
					 * Permet de r�cup�rer les ids clients utilit�s reprends le compteur de client
					 * la ou il �tait
					 */
					Panneau.nbrpan = Integer.parseInt(racine.getAttribute("nbPan"));

					// On nettoie la liste avant de rajouter les panneaux
					ListePan.clear();

					for (int i = 0; i < nbRacineNoeuds; i++) {
						if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
							final Element panneau = (Element) racineNoeuds.item(i);

							// R�cup�ration du panneau

							final Element id = (Element) panneau.getElementsByTagName("id").item(0);
							final Element tarif = (Element) panneau.getElementsByTagName("tarif").item(0);
							final Element visibilite = (Element) panneau.getElementsByTagName("visibilite").item(0);
							final Element longueur = (Element) panneau.getElementsByTagName("longueur").item(0);
							final Element largeur = (Element) panneau.getElementsByTagName("largeur").item(0);
							final Element details = (Element) panneau.getElementsByTagName("details").item(0);
							final NodeList locations = panneau.getElementsByTagName("location");
							final NodeList coords_x = panneau.getElementsByTagName("x");
							final NodeList coords_y = panneau.getElementsByTagName("y");
							final Element x = (Element) coords_x.item(0);
							final Element y = (Element) coords_y.item(0);
							final int nbLocations = locations.getLength();
							final ArrayList<Location> locList = new ArrayList<Location>();
							// reg�n�ration de toutes les locations � partir des ID
							for (int j = 0; j < nbLocations; j++) {

								final Element location = (Element) locations.item(j);

								Location l = ListeLocations
										.retourneLocation(Integer.parseInt(location.getAttribute("id")));
								locList.add(l);
								// System.out.println(l.toString());

							}
							// System.out.println(i+" :"+x.getTextContent());
							ListePan.add(new Panneau(Double.valueOf(tarif.getTextContent()),
									new Visibilite(Integer.parseInt(visibilite.getTextContent())),
									Integer.parseInt(longueur.getTextContent()),
									Integer.parseInt(largeur.getTextContent()), Integer.parseInt(id.getTextContent()),
									locList, details.getTextContent(), Double.valueOf(x.getTextContent()),
									Double.valueOf(y.getTextContent())));
						}
					}
					success = true;
					System.out.println("J'ai refait " + ListePan.size() + " panneaux");
					ListeLocations.recupererLocations();

				} catch (final ParserConfigurationException e) {
					e.printStackTrace();
				} catch (final SAXException e) {
					e.printStackTrace();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					file.createNewFile();
					enregistrerPanneaux();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return success;
	}

	/**
	 * @return the listePan
	 */
	public static ArrayList<Panneau> getListePanneaux() {
		return ListePan;
	}

	/**
	 * @param listePan the listePan to set
	 */
	public static void setListePanneaux(ArrayList<Panneau> listePan) {
		ListePan = listePan;
	}
}
