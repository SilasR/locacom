package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import fr.locacom.main.Principal;
import fr.locacom.model.Client;
import fr.locacom.model.ListeClients;

/**
 * Controller de la fenetre modification d'un client
 */
public class ModifClientController implements Initializable {

	/** Stage de la fenetre modification client */
	private Stage stage;

	@FXML
	private Button valider;

	@FXML
	private TextField adresse;

	@FXML
	private TextField nom;
	@FXML
	private TextField telephone;
	@FXML
	private Label info;
	
	@FXML
	private Text lbTitre;
	@FXML
	private Button bnAnnuler;
	/**
	 * Client pour lequel il faut effectue la modification
	 * 
	 * @see Client
	 */
	private Client client;

	/**
	 * Constructeur qui charge la scene "ajouterClient" depuis le .fxml
	 * 
	 * Avec en parametre le client a modifier pour remplir les champs
	 * @param c : le client qu'il faut modiofié
	 */
	public ModifClientController(Client c) {
		stage = new Stage();
		client = c;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/ajoutClient.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - modif clients");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode pour modifier un client
	 */
	private void modifClient() {

		ListeClients.modifierClient(client.getId(), nom.getText(), adresse.getText(),telephone.getText());
		Principal.fermerFenetreModifClients();
		ListeClientsController.refresh();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		/* On désative le bouton valider si les champs sont vides */
		valider.disableProperty().bind(
			    Bindings.isEmpty(nom.textProperty()).or(Bindings.isEmpty(adresse.textProperty()))
			);
		valider.setOnAction(e -> modifClient());
		adresse.setText(client.getCoord());
		telephone.setText(String.valueOf(client.getTelephone()));
		nom.setText(client.getNom());
		lbTitre.setText("MODIFIER UN CLIENT");
		bnAnnuler.setOnAction(e->Principal.fermerFenetreModifClients());
		
		
	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the valider
	 */
	public Button getValider() {
		return valider;
	}

	/**
	 * @param valider the valider to set
	 */
	public void setValider(Button valider) {
		this.valider = valider;
	}

	/**
	 * @return the adresse
	 */
	public TextField getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(TextField adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the nom
	 */
	public TextField getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(TextField nom) {
		this.nom = nom;
	}

	/**
	 * @return the info
	 */
	public Label getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(Label info) {
		this.info = info;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

}
