package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import fr.locacom.model.Date;
import fr.locacom.main.Principal;
import fr.locacom.model.Client;
import fr.locacom.model.ListeClients;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.Location;
import fr.locacom.model.Panneau;

/** Controler de la fenetre d'ajout client */
public class ModifLocationController implements Initializable {
	/** stage fenetre ajout location */
	private Stage stage;

	/**
	 * location
	 */
	private static Location loc;
	/*
	 * Contient le panneau qu'il faut louer
	 */
	public static Panneau panneau;
	@FXML
	private Label DateDebutLabel;

	@FXML
	private DatePicker DateDebutPicker;

	@FXML
	private Label DateFinLabel;

	@FXML
	private DatePicker DateFinPicker;

	@FXML
	private Label ClientLabel;

	@FXML
	private ChoiceBox<Client> ClientDropdown;

	@FXML
	private Label lbErreur;

	@FXML
	private Button bnAnnuler;
	@FXML
	private Button bnValider;
	@FXML
	private Button bnAjoutClient;
	@FXML
	private Text lbTitre;	

	/**
	 * Constructeur qui charge la scene "ajouterlocation" depuis le .fxml
	 */
	public ModifLocationController(Location l) {
		loc = l;
		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/ajoutLocation.fxml"));

			loader.setController(this);
			stage.setResizable(false);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - modif location");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Méthode pour rafraichire la liste de client
	 */
	public void refresh(){
		ClientDropdown.getItems().clear();
		for (Client c : ListeClients.getListeClients()) {
			ClientDropdown.getItems().add(c);
		}
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		DateDebutPicker.setValue(loc.getDateDebut().toLocal());
		DateFinPicker.setValue(loc.getDateFin().toLocal());

		refresh();
		if (ClientDropdown.getItems().size() != 0) {
			ClientDropdown.setValue(ClientDropdown.getItems().get(0));
		} else {
			bnValider.setDisable(true);
		}

		DateDebutPicker.valueProperty().addListener((v, oldValue, newValue) -> {
			Date date_debut = Date.convert(newValue);
			Date date_fin = Date.convert(DateFinPicker.getValue());
			
			//V�rifie si la date de d�but est inferieure � la date d'aujourd'hui
			if(date_debut.comparerDate(Date.convert(LocalDate.now()))>0) {
				//Sinon annuler le changement
				DateDebutPicker.setValue(oldValue);
			}
			//V�rifie si la date de d�but est inferieure � la date de fin
			if(date_debut.comparerDate(date_fin)<0) {
				//Sinon ajuster la date de fin
				DateFinPicker.setValue(newValue);
			}
		});

		DateFinPicker.valueProperty().addListener((v, oldValue, newValue) -> {
			Date date_debut = Date.convert(newValue);
			Date date_fin = Date.convert(DateDebutPicker.getValue());
			
			//V�rifie si la date de fin est superieure � la date de d�but
			if(date_debut.comparerDate(date_fin)>0) {
				//V�rifie si la date de d�but est inferieure � la date d'aujourd'hui
				if(date_debut.comparerDate(Date.convert(LocalDate.now()))>0) {
					//Sinon annuler le changement
					DateDebutPicker.setValue(oldValue);
				} else {
					//Sinon ajuster la date de d�but
					DateDebutPicker.setValue(newValue);
				}
			}
		});
		bnAjoutClient.setOnAction(e->{
			
			Principal.ouvrirFenetreAjoutClient();
		});
		bnValider.setOnAction(e -> {

			try {
				modifLocation();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		bnAnnuler.setOnAction(e -> {
			stage.close();
		});
		lbTitre.setText("MODIFIER UNE LOCATION");
	}

	/**
	 * @throws ParseException
	 * 
	 * Méthode pour validr la modification d'une location
	 */
	private void modifLocation() throws ParseException {
		Date date_debut = Date.convert(DateDebutPicker.getValue());
		Date date_fin = Date.convert(DateFinPicker.getValue());
		int duree = date_debut.getDuree(date_fin);
		ListeLocations.modifierClient(loc.getIdLoca(), date_debut ,date_fin,duree,ClientDropdown.getSelectionModel().getSelectedItem());
		Principal.fermerFenetreModifLocation();
		ListeLocationsController.refresh();

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}
}
