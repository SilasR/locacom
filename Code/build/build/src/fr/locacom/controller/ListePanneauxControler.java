package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import fr.locacom.main.Principal;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.ListePanneaux;
import fr.locacom.model.Location;
import fr.locacom.model.Panneau;
import fr.locacom.model.Visibilite;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/** Controler de la fenetre liste panneaux */
public class ListePanneauxControler implements Initializable {

	/** Stage de liste panneaux */
	private Stage stage;
	/**
	 * Controller du menu principal pour avoir accès aux méthodes pour supprimer les
	 * panneaux
	 */
	private static MenuPrincipalController menu;
	@FXML
	private TableView<Panneau> tablePanneaux;

	@FXML
	private TableColumn<Panneau, Integer> panneaux_id;

	@FXML
	private TableColumn<Panneau, Integer> panneaux_tarif;

	@FXML
	private TableColumn<Panneau, Visibilite> panneaux_visibilite;

	@FXML
	private TableColumn<Panneau, String> panneaux_detail;

	@FXML
	private TableColumn<Panneau, Integer> panneaux_longueur;

	@FXML
	private TableColumn<Panneau, Integer> panneaux_largeur;
	@FXML
	private TableColumn<Panneau, Integer> panneaux_edit;
	@FXML
	private TableColumn<Panneau, Integer> panneaux_delete;
	@FXML
	private Button bnClose;
	@FXML
	private Button panneaux_add;
	@FXML
	private Button bnDelete;
	private static ObservableList<Panneau> panneaux = FXCollections.observableArrayList();

	/**
	 * Constructeur qui charge la scene "listePanneaux" depuis le .fxml et on garde
	 * en variable le controleur de la fenetre principal
	 * 
	 * @param mC permet d'acceder aux methodes du menu principal et à la carte
	 */
	public ListePanneauxControler(MenuPrincipalController mC) {
		menu = mC;

		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/listePanneaux.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - panneaux");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		refresh();
		// on initialise les colonnes
		panneaux_id.setCellValueFactory(new PropertyValueFactory<>("id"));
		panneaux_tarif.setCellValueFactory(new PropertyValueFactory<>("tarif"));
		panneaux_visibilite.setCellValueFactory(new PropertyValueFactory<>("visibilite"));
		panneaux_detail.setCellValueFactory(new PropertyValueFactory<>("detail"));
		panneaux_longueur.setCellValueFactory(new PropertyValueFactory<>("longueur"));
		panneaux_largeur.setCellValueFactory(new PropertyValueFactory<>("largeur"));
		panneaux_detail.setCellValueFactory(new PropertyValueFactory<>("details"));
		panneaux_edit.setCellValueFactory(new PropertyValueFactory<>("edit"));
		panneaux_delete.setCellValueFactory(new PropertyValueFactory<>("delete"));
		tablePanneaux.setItems(getPanneaux());
		bnDelete.setDisable(true);
		tablePanneaux.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tablePanneaux.setOnMouseClicked(e -> {
			// Si on a selectionné plus de 1 panneauxx
			if (tablePanneaux.getSelectionModel().getSelectedItems().size() > 1) {
				bnDelete.setDisable(false);
			} else {
				bnDelete.setDisable(true);
			}
		});
		bnDelete.setOnAction(e -> {
			for (Panneau p : tablePanneaux.getSelectionModel().getSelectedItems()) {
				gererDelete(p);
			}
			bnDelete.setDisable(true);
		});
		bnClose.setOnAction(e -> {

			this.stage.close();
		});
		panneaux_add.setOnAction(e -> {
			Principal.ouvrirFenetreAjoutPanneau();
		});

	}

	/**
	 * Méthode pour raffraichir la liste des panneaux
	 */
	public static void refresh() {
		// On ajout tous les panneaux dans l'observable list
		panneaux.clear();
		for (Panneau p : ListePanneaux.getListePanneaux()) {
			panneaux.add(new Panneau(p.getTarif(), p.getVisibilite(), p.getLongueur(), p.getLargeur(), p.getId(),
					p.getLocations(), p.getDetails(),p.getX(),p.getY()));
		}
	}

	/**
	 * Methode pour gerer l'edition d'un panneau
	 *
	 * @param p le panneau a edité
	 */
	public static void gererEdit(Panneau p) {

		/* On ouvre la fenetre de modification avec en parametre le panneau */
		Principal.ouvrirFenetreModifPanneau(p);

	}

	/**
	 * Methode pour gerer la suppresion d'un panneau
	 * 
	 * @param p le panneau a supprimé
	 */
	public static void gererDelete(Panneau p) {
		/* On demande confirmation a l'utilisateur */
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation");
		alert.setHeaderText("");
		alert.setContentText("Etes vous sur de vouloir supprimer ce panneau : " + p.getId());

		Optional<ButtonType> result = alert.showAndWait();
		/* On regarde sa reponse */
		if (result.get() == ButtonType.OK) {
			System.out.println("suppression de : " + p.toString());
			/* pour toutes locations du panneau P */
			for(Location l : ListeLocations.retourneLocation(p)) {
			
				/* on les supprimes */
				ListeLocations.supprimerLocation(l.getIdLoca());
			
			}
			// On le supprime de l'observable liste
			// on le supprime des fichiers
			ListePanneaux.supprimerPanneau(p.getId());
			panneaux.remove(p);
			/* On l'enlève de la carte */
			MenuPrincipalController.setSupprimerPanneau(false);
			Principal.refreshMenu();

		}

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the menu
	 */
	public static MenuPrincipalController getMenu() {
		return menu;
	}

	/**
	 * @param menu the menu to set
	 */
	public static void setMenu(MenuPrincipalController menu) {
		ListePanneauxControler.menu = menu;
	}

	/**
	 * @return the tablePanneaux
	 */
	public TableView<Panneau> getTablePanneaux() {
		return tablePanneaux;
	}

	/**
	 * @param tablePanneaux the tablePanneaux to set
	 */
	public void setTablePanneaux(TableView<Panneau> tablePanneaux) {
		this.tablePanneaux = tablePanneaux;
	}

	/**
	 * @return the panneaux_id
	 */
	public TableColumn<Panneau, Integer> getPanneaux_id() {
		return panneaux_id;
	}

	/**
	 * @param panneaux_id the panneaux_id to set
	 */
	public void setPanneaux_id(TableColumn<Panneau, Integer> panneaux_id) {
		this.panneaux_id = panneaux_id;
	}

	/**
	 * @return the panneaux_tarif
	 */
	public TableColumn<Panneau, Integer> getPanneaux_tarif() {
		return panneaux_tarif;
	}

	/**
	 * @param panneaux_tarif the panneaux_tarif to set
	 */
	public void setPanneaux_tarif(TableColumn<Panneau, Integer> panneaux_tarif) {
		this.panneaux_tarif = panneaux_tarif;
	}

	/**
	 * @return the panneaux_visibilite
	 */
	public TableColumn<Panneau, Visibilite> getPanneaux_visibilite() {
		return panneaux_visibilite;
	}

	/**
	 * @param panneaux_visibilite the panneaux_visibilite to set
	 */
	public void setPanneaux_visibilite(TableColumn<Panneau, Visibilite> panneaux_visibilite) {
		this.panneaux_visibilite = panneaux_visibilite;
	}

	/**
	 * @return the panneaux_detail
	 */
	public TableColumn<Panneau, String> getPanneaux_detail() {
		return panneaux_detail;
	}

	/**
	 * @param panneaux_detail the panneaux_detail to set
	 */
	public void setPanneaux_detail(TableColumn<Panneau, String> panneaux_detail) {
		this.panneaux_detail = panneaux_detail;
	}

	/**
	 * @return the panneaux_longueur
	 */
	public TableColumn<Panneau, Integer> getPanneaux_longueur() {
		return panneaux_longueur;
	}

	/**
	 * @param panneaux_longueur the panneaux_longueur to set
	 */
	public void setPanneaux_longueur(TableColumn<Panneau, Integer> panneaux_longueur) {
		this.panneaux_longueur = panneaux_longueur;
	}

	/**
	 * @return the panneaux_largeur
	 */
	public TableColumn<Panneau, Integer> getPanneaux_largeur() {
		return panneaux_largeur;
	}

	/**
	 * @param panneaux_largeur the panneaux_largeur to set
	 */
	public void setPanneaux_largeur(TableColumn<Panneau, Integer> panneaux_largeur) {
		this.panneaux_largeur = panneaux_largeur;
	}

	/**
	 * @return the panneaux_edit
	 */
	public TableColumn<Panneau, Integer> getPanneaux_edit() {
		return panneaux_edit;
	}

	/**
	 * @param panneaux_edit the panneaux_edit to set
	 */
	public void setPanneaux_edit(TableColumn<Panneau, Integer> panneaux_edit) {
		this.panneaux_edit = panneaux_edit;
	}

	/**
	 * @return the panneaux_delete
	 */
	public TableColumn<Panneau, Integer> getPanneaux_delete() {
		return panneaux_delete;
	}

	/**
	 * @param panneaux_delete the panneaux_delete to set
	 */
	public void setPanneaux_delete(TableColumn<Panneau, Integer> panneaux_delete) {
		this.panneaux_delete = panneaux_delete;
	}

	/**
	 * @return the bnClose
	 */
	public Button getBnClose() {
		return bnClose;
	}

	/**
	 * @param bnClose the bnClose to set
	 */
	public void setBnClose(Button bnClose) {
		this.bnClose = bnClose;
	}

	/**
	 * @return the panneaux_add
	 */
	public Button getPanneaux_add() {
		return panneaux_add;
	}

	/**
	 * @param panneaux_add the panneaux_add to set
	 */
	public void setPanneaux_add(Button panneaux_add) {
		this.panneaux_add = panneaux_add;
	}

	/**
	 * @return the bnDelete
	 */
	public Button getBnDelete() {
		return bnDelete;
	}

	/**
	 * @param bnDelete the bnDelete to set
	 */
	public void setBnDelete(Button bnDelete) {
		this.bnDelete = bnDelete;
	}

	/**
	 * @return the panneaux
	 */
	public static ObservableList<Panneau> getPanneaux() {
		return panneaux;
	}

	/**
	 * @param panneaux the panneaux to set
	 */
	public static void setPanneaux(ObservableList<Panneau> panneaux) {
		ListePanneauxControler.panneaux = panneaux;
	}

}
