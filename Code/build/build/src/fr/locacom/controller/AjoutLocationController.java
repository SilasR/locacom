package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import fr.locacom.model.Date;
import fr.locacom.main.Principal;
import fr.locacom.model.Client;
import fr.locacom.model.ListeClients;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.Location;
import fr.locacom.model.Panneau;

/** Controler de la fenetre d'ajout client */
public class AjoutLocationController implements Initializable {
	/** stage fenetre ajout location */
	private Stage stage;
	/*
	 * variable pour savoir si on est dans l'ajout d'une location permet de savoir
	 * si il faut detecter le click sur un panneau
	 */
	public static boolean addLocation = false;
	/**
	 * Controller du menu principal pour avoir accès aux méthodes pour supprimer les
	 * panneaux
	 */
	private static MenuPrincipalController menu;
	/*
	 * Contient le panneau qu'il faut louer
	 */
	public static Panneau panneau;
	@FXML
	private Label DateDebutLabel;

	@FXML
	private DatePicker DateDebutPicker;

	@FXML
	private Label DateFinLabel;

	@FXML
	private DatePicker DateFinPicker;

	@FXML
	private Label ClientLabel;

	@FXML
	private ChoiceBox<Client> ClientDropdown;

	@FXML
	private Label lbErreur;

	@FXML
	private Button bnAnnuler;
	@FXML
	private Button bnValider;
	@FXML
	private Button bnAjoutClient;

	/**
	 * Constructeur qui charge la scene "ajouterlocation" depuis le .fxml
	 */
	public AjoutLocationController(MenuPrincipalController mp) {
		menu = mp;
		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/ajoutLocation.fxml"));

			loader.setController(this);
			stage.setResizable(false);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - ajout location");
			/* Evenements du claviers */
			stage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				/* si on appuie szur ENTREE */
				if (event.getCode() == KeyCode.ENTER) {
					/* et que les champs sont OK */
					if (!bnValider.isDisabled()) {
						ajoutLocation();
						event.consume();
					}
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode pour rafraichir le dropdown de client
	 */
	public void refresh() {
		ClientDropdown.getItems().clear();
		for (Client c : ListeClients.getListeClients()) {
			ClientDropdown.getItems().add(c);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		/* ON initialise les dates picker à la date du jour */
		DateDebutPicker.setValue(LocalDate.now());
		DateFinPicker.setValue(LocalDate.now());

		refresh();
		if (ClientDropdown.getItems().size() != 0) {
			ClientDropdown.setValue(ClientDropdown.getItems().get(0));
		} else {
			/* Si aucun client alors on desactive le bouton valider */
			bnValider.setDisable(true);
		}

		DateDebutPicker.valueProperty().addListener((v, oldValue, newValue) -> {
			Date date_debut = Date.convert(newValue);
			Date date_fin = Date.convert(DateFinPicker.getValue());

			// V�rifie si la date de d�but est inferieure � la date d'aujourd'hui
			if (date_debut.comparerDate(Date.convert(LocalDate.now())) < 0) {
				//Vérifie si la date de début est inférieure à la date de fin
				if (date_debut.comparerDate(date_fin)<0) {
					// Sinon ajuster la date de fin
					DateFinPicker.setValue(newValue);
				}
			} else {
				// Sinon annuler le changement
				DateDebutPicker.setValue(oldValue);
			}
		});

		DateFinPicker.valueProperty().addListener((v, oldValue, newValue) -> {
			Date date_debut = Date.convert(newValue);
			Date date_fin = Date.convert(DateDebutPicker.getValue());

			// V�rifie si la date de fin est superieure � la date de d�but
			if (date_debut.comparerDate(date_fin) > 0) {
				// V�rifie si la date de d�but est inferieure � la date d'aujourd'hui
				if (date_debut.comparerDate(Date.convert(LocalDate.now()))  > 0) {
					// Sinon annuler le changement
					DateFinPicker.setValue(oldValue);
					
				} else {
					// Sinon ajuster la date de d�but
					DateDebutPicker.setValue(newValue);
				}
			}
		});
		bnAjoutClient.setOnAction(e -> {

			Principal.ouvrirFenetreAjoutClient();
		});
		bnValider.setOnAction(e -> {

			ajoutLocation();

		});
		bnAnnuler.setOnAction(e -> {
			stage.close();
			addLocation = false;
		});

	}

	/**
	 * méthode pour ajouter une location
	 */
	private void ajoutLocation() {
		addLocation = false;
		Date date_debut = Date.convert(DateDebutPicker.getValue());
		Date date_fin = Date.convert(DateFinPicker.getValue());
		int duree = date_debut.getDuree(date_fin);
		Location l = new Location(duree, ClientDropdown.getSelectionModel().getSelectedItem(), panneau, date_debut,
				date_fin);
		ListeLocations.getListeLocations().add(l);
		panneau.getLocations().add(l);
		ListeLocationsController.refresh();
		ListeClientsController.refresh();
		stage.close();
		menu.placerPanneaux();

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}
}
