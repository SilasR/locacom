package fr.locacom.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * Classe qui definit le contenu de la fenetre Aide
 * @see Stage
 *
 */

public class FenAide extends Stage {
	private Label label_titre = new Label("Besoin d'aide ?");
	private Label label_text = new Label("Voici un tutoriel pour vous aider à utiliser ce logiciel.\n\n"
			+ "1) Pour ajouter un client cliquer sur \"Ajouter\" puis sélectionner client. Vous devrez obligatoirement indiquer son nom et son adresse, puis si vous le souhaitez son numéro de téléphone \n\n"
			+ "2) Pour ajouter un panneau cliquer sur \"Ajouter\" puis sélectionner panneau. Vous devrez saisir les différentes informations puis sélectionner un endroit ou placer le panneau \n\n"
			+ "3) Pour ajouter une location cliquer sur \"Ajouter\" puis sélectionner location. Vous allez devoir sélectionner un panneau à louer puis renseigner le client et les dates de locations \n\n"
			+ "4) Vous pouvez consulter les clients, panneaux et locations dans \"Liste\" \n\n"
			+ "5) La supression d'un panneau se fait via le bouton \"Supprimer\" puis \"Panneau\", cette action engendre la suppresion de toutes les réservations associés ! \n\n"
			+ "6) La suppresion d'une location se fait via la liste des locations tout comme la suppresion d'un client.\n\n"
			+ "NB : tant que les informations ne sont pas valides vous ne pourrer pas ajouter un panneau, client ou une location");

	/**
	 * Constructeur qui va iinstancier une fenetre d'aide
	 */
	public FenAide() {
		this.setTitle("Aide");

		this.setResizable(false);
		Scene laScene = new Scene(creerContenu(), 400, 600);
		this.setScene(laScene);
		this.sizeToScene();
	}


	/**
	 * Definition des elements contenu dans la fenetre
	 * 
	 * @return Border Pane correspondant a la fenetre
	 */
	Parent creerContenu() {
		BorderPane BP = new BorderPane();
		label_titre.setFont(Font.font(20));
		BorderPane.setAlignment(label_titre, Pos.CENTER);
		BP.setTop(label_titre);
		ScrollPane sp = new ScrollPane();
		sp.setContent(label_text);
		label_text.setPadding(new Insets(20, 10, 20, 10));
		sp.fitToWidthProperty().setValue(true);
		label_text.setWrapText(true);
		BP.setCenter(sp);

		return BP;
	}

}
