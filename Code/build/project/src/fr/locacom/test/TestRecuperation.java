/**
 * 
 */
package fr.locacom.test;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;

import fr.locacom.model.ListeClients;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.ListePanneaux;

/**
 * Cette classe permet de tester la récupération
 * des panneaux, des locations et des clients
 */
public class TestRecuperation{

	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * Ce test vérifie la récupération des clients
	 */
	@Test
	public void recuperationClients() {
		//On vérifie que la récupération de clients n'a généré aucune erreur
		assertTrue(ListeClients.recupererClients());
	}
	
	/**
	 * Ce test vérifie la récupération des locations
	 */
	@Test
	public void recuperationLocations() throws DOMException, ParseException {
		//On vérifie que la récupération de locations n'a généré aucune erreur
		assertTrue(ListeLocations.recupererLocations());
	}
	
	/**
	 * Ce test vérifie la récupération des panneaux
	 */
	@Test
	public void recuperationPanneaux() {
		//On vérifie que la récupération de panneaux n'a généré aucune erreur
		assertTrue(ListePanneaux.recupererPanneaux());
	}

}
