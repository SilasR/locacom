package fr.locacom.main;

import java.text.ParseException;

import org.w3c.dom.DOMException;

import fr.locacom.controller.AjoutClientController;
import fr.locacom.controller.AjoutLocationController;
import fr.locacom.controller.AjoutPanneauController;
import fr.locacom.controller.ListeClientsController;
import fr.locacom.controller.ListeLocationsController;
import fr.locacom.controller.ListePanneauxControler;
import fr.locacom.controller.MenuPrincipalController;
import fr.locacom.controller.ModifClientController;
import fr.locacom.controller.ModifLocationController;
import fr.locacom.controller.ModifPanneauController;
import fr.locacom.model.Client;
import fr.locacom.model.Location;
import fr.locacom.model.Panneau;
import fr.locacom.view.FenAide;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * 
 * Classe principale qui initialise les fenêtres au démarage de l'application et
 * qui gère différentes actions liées à l'éxécution du programme tel que
 * l'ouverture de nouvelles fenetres
 * 
 */

public class Principal extends Application {

	/** Fenêtre du menu principale */
	static private MenuPrincipalController fenMenuPrincipal = new MenuPrincipalController();
	/** Fenêtre liste client */
	static private ListeClientsController fenListeClients;
	/** Fenêtre liste location */
	static private ListeLocationsController fenListeLocations;
	/** Fenêtre liste panneaux */
	static private ListePanneauxControler fenListePanneaux;
	/** Fenêtre ajout d'un client */
	static private AjoutClientController fenAjoutClient;
	/** Fenêtre ajout d'un panneau */
	static private AjoutPanneauController fenAjoutPanneau;
	/** Fenêtre ajout d'une location */
	static private AjoutLocationController fenAjoutLocation;
	/** Fenêtre modification d'une client */
	static private ModifClientController fenModifClient;
	/** Fenêtre modification d'un panneau */
	static private ModifPanneauController fenModifPanneau;
	/** Fenêtre modification d'une location */
	static private ModifLocationController fenModifLocation;


	static private FenAide fenAide = new FenAide();

	/**
	 * Méthode lancée au démarrage de l'application
	 * 
	 * @param args - Arguments passés en paramètre lors de l'appel du programme
	 */

	public static void main(String[] args) throws DOMException, ParseException {

		launch(args);
	}

	/**
	 * Méthode qui affiche une fenêtre principale
	 * 
	 * @param primaryStage - La fenêtre à afficher
	 */

	public void start(Stage primaryStage) {
		fenMenuPrincipal.getStage().show();

	}

	/**
	 * Méthode pour afficher la fenêtre liste clients
	 */
	public static void ouvrirFenetreListeClients() {
		fenListeClients = new ListeClientsController();
		fenListeClients.getStage().show();
	}
	/**
	 * Méthode pour fermer la fenêtre liste clients
	 */
	public static void fermerFenetreListeClients() {
		fenListeClients.getStage().close();
	}
	/**
	 * Méthode pour afficher la fenêtre liste locations
	 */
	public static void ouvrirFenetreListeLocations() {
		fenListeLocations = new ListeLocationsController(fenMenuPrincipal);
		fenListeLocations.getStage().show();
	}
	/**
	 * Méthode pour fermer la fenêtre liste locations
	 */
	public static void fermerFenetreListeLocations() {
		fenListeLocations.getStage().close();
	}

	/**
	 * Méthode pour afficher la fenêtre du modification d'un client
	 * 
	 * @param c : Le client que le souhaite modifié
	 */
	public static void ouvrirFenetreModifClients(Client c) {
		fenModifClient = new ModifClientController(c);
		fenModifClient.getStage().show();
		fenModifClient.getStage().setOnCloseRequest(event -> ListeClientsController.refresh());
	}

	/**
	 * Méthode pour fermer la fenêtre modification client
	 */
	public static void fermerFenetreModifClients() {
		fenModifClient.getStage().close();
	}

	/**
	 * Méthode pour ouvrir la fenêtre de modification d'une location
	 * 
	 * @param l : la location que l'on souhaite modifié
	 */
	public static void ouvrirFenetreModifLocation(Location l) {
		fenModifLocation = new ModifLocationController(l);
		fenModifLocation.getStage().show();
		fenModifLocation.getStage().setOnCloseRequest(event -> ListeLocationsController.refresh());
	}

	/**
	 * Méthode pour fermer la fenêtre de modification d'une location
	 */
	public static void fermerFenetreModifLocation() {
		fenModifLocation.getStage().close();
	}
	/**
	 * M&thode pour ouvrir la fenêtre de modification d'un panneau
	 * 
	 * @param p : la panneau a modisifé
	 */
	public static void ouvrirFenetreModifPanneau(Panneau p) {
		fenModifPanneau = new ModifPanneauController(p);
		fenModifPanneau.getStage().show();
		fenModifPanneau.getStage().setOnCloseRequest(event -> ListeClientsController.refresh());
	}

	/**
	 * Méthode pour fermer la fenêtre de modification d'un panneau
	 */
	public static void fermerFenetreModifPanneau() {
		fenModifPanneau.getStage().close();
	}

	/**
	 * Méthode pour ouvrir la fenêtre d'ajout d'un panneau
	 */
	public static void ouvrirFenetreAjoutPanneau() {
		fenAjoutPanneau = new AjoutPanneauController();
		AjoutPanneauController.getStage().show();
	}

	/**
	 * Méthode pour fermer la fenêtre d'ajout d'un panneau
	 */
	public static void fermerFenetreAjoutPanneau() {
		AjoutPanneauController.getStage().close();
	}

	/**
	 * Méthode pour ouvrir la fenêtre d'ajout d'une location
	 */
	public static void ouvrirFenetreAjoutLocation() {
		fenAjoutLocation = new AjoutLocationController(fenMenuPrincipal);
		fenAjoutLocation.getStage().show();
	}
	/**
	 * Méthode pour fermer la fenêtre d'ajout d'une location
	 */
	public static void fermerFenetreAjoutLocation() {
		fenAjoutLocation.getStage().close();
	}
	/**
	 * Méthode pour ouvrir la fenêtre d'ajout d'un client
	 */
	public static void ouvrirFenetreAjoutClient() {
		fenAjoutClient = new AjoutClientController();
		fenAjoutClient.getStage().show();
	}
	/**
	 * Méthode pour fermer la fenêtre d'ajout d'un client
	 */
	public static void fermerFenetreAjoutClient() {
		fenAjoutClient.getStage().close();
	}

	/**
	 * Méthode pour ouvrir le menu principal
	 */
	public static void ouvrirMenu() {
		fenMenuPrincipal.getStage().show();
	}

	/**
	 * Méthode pour fermer le menu principal
	 */
	public static void fermerMenu() {
		fenMenuPrincipal.getStage().close();
	}

	/**
	 * Méthode pour rafraichir le menu principal (on replace les panneaux)
	 */
	public static void refreshMenu() {
		fenMenuPrincipal.placerPanneaux();
	}

	/**
	 * Méthode pour ouvrir la fenêtre liste de panneaux
	 */
	public static void ouvrirFenetreListePanneaux() {
		fenListePanneaux = new ListePanneauxControler(fenMenuPrincipal);
		// On l'affiche
		fenListePanneaux.getStage().show();
	}
	/**
	 * Méthode pour afficher la enêtre d'aide
	 */
	public static void ouvrirAide() {
		fenAide.show();
	}


}
