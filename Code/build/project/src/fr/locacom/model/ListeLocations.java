package fr.locacom.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.locacom.controller.ListeLocationsController;

/**
 * Classe permettant de gerer une liste de locations et son enregistrement dans
 * un fichier .xml
 */
public class ListeLocations {

	/** ArrayList qui contient toute les location */
	private static ArrayList<Location> ListeLoca = new ArrayList<Location>();

	/**
	 * @param l
	 * 
	 *          Methode pour ajouter une location dans la liste
	 */
	public void ajouterLoca(Location l) {
		ListeLoca.add(l);
	}

	/**
	 * @param id
	 * 
	 *           Méthode pour supprimer une location grace a son ID
	 */
	public static void supprimerLocation(int id) {
		Iterator<Location> itr = ListeLoca.iterator();
		while (itr.hasNext()) {
			Location l = itr.next();
			if (l.getIdLoca() == id) {
				itr.remove();
				ListeLocationsController.refresh();
			}
		}

	}

	/**
	 * @param id
	 * @return {@link Location}
	 * 
	 *         Methode qui retourne la location avec l'ID en parametre ou NULL si
	 *         non trouve
	 */
	public static Location retourneLocation(int id) {
		Location location = null;
		for (Location l : ListeLoca) {
			if (l.getIdLoca() == id) {
				location = l;
			}
		}
		return location;
	}

	public static ArrayList<Location> retourneLocation(Panneau p) {
		ArrayList<Location> location = new ArrayList<Location>();
		for (Location l : ListeLoca) {
			if (l.getPanneau().getId() == p.getId()) {
				location.add(l);
			}
		}
		return location;
	}

	/**
	 * @param id
	 * @param dateD
	 * @param dateF
	 * @param client
	 * 
	 *               Methode pour modifier une location grace a son id
	 */
	public static void modifierClient(int id, Date dateD, Date dateF, int duree, Client c) {
		Location l = retourneLocation(id);
		ListeLoca.get(ListeLoca.indexOf(l)).setDateDebut(dateD);
		ListeLoca.get(ListeLoca.indexOf(l)).setDateFin(dateF);
		ListeLoca.get(ListeLoca.indexOf(l)).setClient(c);
		ListeLoca.get(ListeLoca.indexOf(l)).setDuree(duree);

		enregistrerLocations();
	}

	/**
	 * Methode permettant d'enregistrer tous les locations dans un fichier XML
	 */
	public static boolean enregistrerLocations() {
		System.out.println("j'enregistre les locations");
		boolean success = false;

		try {

			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();

			// cr�ation document
			final Document document = builder.newDocument();

			final Element racine = document.createElement("locations");
			// Sauvgarde du compteur d'id clients pour avoir un ID unique
			racine.setAttribute("cpt", Integer.toString(Location.cpt));
			document.appendChild(racine);

			// Enregistrement de chaque client
			for (Location l : ListeLoca) {

				final Element location = document.createElement("location");

				racine.appendChild(location);

				final Element id = document.createElement("id");
				id.appendChild(document.createTextNode(Integer.toString(l.getIdLoca())));

				final Element duree = document.createElement("duree");
				duree.appendChild(document.createTextNode(Integer.toString(l.getDuree())));

				final Element client = document.createElement("client_id");
				client.appendChild(document.createTextNode(Integer.toString(l.getClient().getId())));

				final Element panneau = document.createElement("panneau_id");
				panneau.appendChild(document.createTextNode(Integer.toString(l.getPanneau().getId())));

				String date_d = l.getDateDebut().toString();
				final Element dateD = document.createElement("dateDebut");
				dateD.appendChild(document.createTextNode(date_d));

				String date_f = l.getDateFin().toString();
				final Element dateF = document.createElement("dateFin");
				dateF.appendChild(document.createTextNode(date_f));

				location.appendChild(id);
				location.appendChild(duree);
				location.appendChild(client);
				location.appendChild(panneau);
				location.appendChild(dateD);
				location.appendChild(dateF);

			}

			final TransformerFactory transformerFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformerFactory.newTransformer();
			final DOMSource source = new DOMSource(document);
			final StreamResult sortie = new StreamResult(new File("locations.xml"));

			transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

			// formatage
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			transformer.transform(source, sortie);
			success = true;
		} catch (final ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return success;
	}

	/**
	 * Methode permettant de recuperer les clients enregistrer dans un fichiers xml
	 * Recupere tous les clients et les remets dans la ListeClient
	 */
	public static boolean recupererLocations() {

		System.out.println("je recupere les locations");
		{
			boolean success = false;
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			/* On regarde si le fichier éxiste */
			File file = new File("locations.xml");
			if (file.exists()) {
				try {

					final DocumentBuilder builder = factory.newDocumentBuilder();
					final Document document = builder.parse(new File("locations.xml"));

					final Element racine = document.getDocumentElement();
					final NodeList racineNoeuds = racine.getChildNodes();
					final int nbRacineNoeuds = racineNoeuds.getLength();
					/*
					 * Permet de r�cup�rer les ids clients utilit�s reprends le compteur de client
					 * la ou il �tait
					 */
					Location.cpt = Integer.parseInt(racine.getAttribute("cpt"));

					// On nettoie la liste avant de rajouter les clients
					ListeLoca.clear();

					for (int i = 0; i < nbRacineNoeuds; i++) {

						if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
							final Element location = (Element) racineNoeuds.item(i);

							// Affichage d'un client

							final Element id = (Element) location.getElementsByTagName("id").item(0);
							final Element duree = (Element) location.getElementsByTagName("duree").item(0);
							final Element client = (Element) location.getElementsByTagName("client_id").item(0);
							final Element panneau = (Element) location.getElementsByTagName("panneau_id").item(0);
							final Element dateD = (Element) location.getElementsByTagName("dateDebut").item(0);
							final Element dateF = (Element) location.getElementsByTagName("dateFin").item(0);
							int d = Integer.parseInt(duree.getTextContent());
							// On r�cup�re le Client gr�ce � son ID
							Client c = ListeClients.retourneClients(Integer.parseInt(client.getTextContent()));
							// On r�cup�re le Panneau gr�ce � son ID
							Panneau p = ListePanneaux.retournePanneau(Integer.parseInt(panneau.getTextContent()));
							String[] date = dateD.getTextContent().split("/");
							Date dateDebut = new Date(Integer.parseInt(date[0]), Integer.parseInt(date[1]),
									Integer.parseInt(date[2]));
							date = dateF.getTextContent().split("/");
							Date dateFin = new Date(Integer.parseInt(date[0]), Integer.parseInt(date[1]),
									Integer.parseInt(date[2]));

							Location loc = new Location(d, c, p, dateDebut, dateFin,
									Integer.parseInt(id.getTextContent()));
							ListeLoca.add(loc);

						}
					}
					success = true;
				} catch (final ParserConfigurationException e) {
					e.printStackTrace();
				} catch (final SAXException e) {
					e.printStackTrace();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					file.createNewFile();
					enregistrerLocations();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return success;
		}

	}

	/**
	 * @return the listeLoca
	 */
	public static ArrayList<Location> getListeLocations() {
		return ListeLoca;
	}

	/**
	 * @param listeLoca the listeLoca to set
	 */
	public static void setListeLocations(ArrayList<Location> listeLoca) {
		ListeLoca = listeLoca;
	}

}
