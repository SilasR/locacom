package fr.locacom.model;

import fr.locacom.controller.ListeClientsController;

/**
 * Classe permettant la creation d'un client avec un nom, des coordonnes etc
 */
public class Client extends Boutons {

	/* Compteur statique du nombre de clients */
	public static int nbrcli = 0;
	/* Nom du client */
	private String nom;
	/* Coordonnes du client */
	private String coord;
	/* Compteur du nombre de reservation du client */
	private int nbResa;
	/* ID du client attribue automatiquement selon le compteur de clients */
	private int id;
	/* Téléphone du client */
	private String telephone;

	/**
	 * @param n
	 * @param c
	 * @param nbResa
	 * @param id
	 * @param edit
	 * @param delete
	 * 
	 *               Construit un client dont on connait deja l'ID 
	 */
	public Client(String n, String c, int nbResa, int id,String telephone) {
		super();
		this.nom = n;
		this.coord = c;
		this.nbResa = nbResa;
		this.id = id;
		this.telephone = telephone;
		super.getEdit().setOnAction(e -> {
			ListeClientsController.gererEdit(this);
		});
		super.getDelete().setOnAction(e -> {
			ListeClientsController.gererDelete(this);
		});

	}
	/**
	 * @param n
	 * @param c
	 * @param nbResa
	 * 
	 *               Construit un nouveau client Avec un id attribue automatiquement
	 */
	public Client(String n, String c) {
		super();
		this.nom = n;
		this.coord = c;
		this.nbResa = 0;
		nbrcli++;
		this.id = nbrcli;
		this.telephone = "";
		
		ListeClients.getListeClients().add(this);
	}

	/**
	 * @param n
	 * @param c
	 * @param nbResa
	 * 
	 *               Construit un nouveau client Avec un id attribue automatiquement
	 */
	public Client(String n, String c,String telephone) {
		super();
		this.nom = n;
		this.coord = c;
		this.nbResa = 0;
		nbrcli++;
		this.id = nbrcli;
		this.telephone = telephone;	
		
		ListeClients.getListeClients().add(this);
	}


	@Override
	public String toString() {
		return this.nom;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the coord
	 */
	public String getCoord() {
		return coord;
	}

	/**
	 * @param coord the coord to set
	 */
	public void setCoord(String coord) {
		this.coord = coord;
	}

	/**
	 * @return the nbResa
	 */
	public int getNbResa() {
		return nbResa;
	}

	/**
	 * @param nbResa the nbResa to set
	 */
	public void setNbResa(int nbResa) {
		this.nbResa = nbResa;
	}

	/**
	 * @return the nbrcli
	 */
	public static int getNbrcli() {
		return nbrcli;
	}

	/**
	 * @param nbrcli the nbrcli to set
	 */
	public static void setNbrcli(int nbrcli) {
		Client.nbrcli = nbrcli;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

}
