package fr.locacom.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Classe permettant de definir une visibilite compose d'un ID et d'un nom
 */
public class Visibilite {

	/* ID de la visibilite */
	private int id;
	/* nom de la visibilite */
	private String nom;
	/*
	 * Obersable list pour les choix de visibilités
	 */
	private static ObservableList<Visibilite> visibilites = FXCollections.observableArrayList(new Visibilite(1),
			new Visibilite(2), new Visibilite(3), new Visibilite(4), new Visibilite(5));

	/**
	 * @param id
	 * @param nom
	 * 
	 *            Construit une visbilite selon un ID et un nom
	 */
	public Visibilite(int id, String nom) {

		this.id = id;
		this.nom = nom;
	}

	/**
	 * @param id
	 * 
	 *           Construit une visbilite selon un ID 1 -> mauvaise 2 -> Moyenne 3 ->
	 *           bonne 4 -> Tres bonne 5 -> Excellente
	 */
	public Visibilite(int id) {
		switch (id) {
		case 1:
			this.id = id;
			this.nom = "Mauvaise";
			break;
		case 2:
			this.id = id;
			this.nom = "Moyenne";
			break;
		case 3:
			this.id = id;
			this.nom = "Bonne";
			break;
		case 4:
			this.id = id;
			this.nom = "Très bonne";
			break;
		case 5:
			this.id = id;
			this.nom = "Excellente";
			break;
		default:
		}

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return  nom;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the visibilites
	 */
	public static ObservableList<Visibilite> getVisibilites() {
		return visibilites;
	}

	/**
	 * @param visibilites the visibilites to set
	 */
	public static void setVisibilites(ObservableList<Visibilite> visibilites) {
		Visibilite.visibilites = visibilites;
	}


}
