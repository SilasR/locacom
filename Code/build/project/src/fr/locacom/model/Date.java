package fr.locacom.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Date {

	private int jour;
	private int mois;
	private int annee;
	

	/**
	 * @param j : le jour
	 * @param m : le lois
	 * @param a : l'année
	 * 
	 * Créer une date 
	 */
	public Date(int j, int m, int a)
	{
		this.jour = j;
		this.mois = m;
		this.annee = a;
	}
	/**
	 * @param l : la date a convertir
	 * @return La date au format Date
	 */
	public static Date convert(LocalDate l) {
		Date date_return = null;
		String date = l.format(DateTimeFormatter.ofPattern("dd/MM/yyyyy"));
		String[] dates = date.split("/");
		date_return = new Date(Integer.parseInt(dates[0]), Integer.parseInt(dates[1]), Integer.parseInt(dates[2]));
		return date_return;
		
	}
	/**
	 * @return la date converti en Localdate
	 * @see LocalDate
	 */
	public LocalDate toLocal() {
		LocalDate a = LocalDate.of(this.annee,this.mois,this.jour);
		return a;

		
	}
	public int getJour()
	{
		return this.jour;
	}
	
	public int getMois()
	{
		return this.mois;
	}
	
	public int getAnnee()
	{
		return this.annee;
	}
	
	public void afficher()
	{
		if(this.jour < 10)
		{
			System.out.print("0");
		}
		System.out.print(this.jour + "/");
		if(this.mois < 10)
		{
			System.out.print("0");
		}
		System.out.print(this.mois + "/");
		System.out.println(this.annee);
	}
	
	/**
	 * @param d : la date dont il faut faire la différence
	 * @return la durée entre les 2 dates
	 * 
	 * Permet de calculé la durée (en jours) entre 2 dates
	 */
	public int getDuree(Date d) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        java.util.Date d1;
        java.util.Date d2;
		try {
			d1 = format.parse(d.toString());
			d2 = format.parse(this.toString());
	        long diffInMillies = Math.abs(d2.getTime() - d1.getTime());
	        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			int i = (int)diff;
			return i;
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int comparerDate(Date d) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        java.util.Date d1;
        java.util.Date d2;
		try {
			d1 = format.parse(d.toString());
			d2 = format.parse(this.toString());
			return d1.compareTo(d2);
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public String toString() {
		String date = "";
		if(this.jour < 10)
		{
			date+=("0");
		}
		date+=(this.jour + "/");
		if(this.mois < 10)
		{
			date+=("0");
		}
		date+=(this.mois + "/");
		date+=(this.annee);
		return date;
	}
	
}
