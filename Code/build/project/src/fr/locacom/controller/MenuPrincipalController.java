package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import fr.locacom.main.*;
import fr.locacom.model.ListeClients;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.ListePanneaux;
import fr.locacom.model.Panneau;
import fr.locacom.view.FenPanneau;

/**
 * Controller de la fenêtre principal du logiciel
 */
public class MenuPrincipalController implements Initializable {

	/** Stage de la fenêtre principal */
	private Stage stage;
	@FXML
	private VBox root;
	@FXML
	private Button b1;
	@FXML
	private Button listerClient;
	@FXML
	private ImageView map;
	@FXML
	private Pane pane_map;
	@FXML
	private ScrollPane scrollpane;
	// variable pour savoir si on est dans le mode de suppression d'un panneau
	private static boolean supprimerPanneau = false;

	/**
	 * Constructeur qui charge la scene "menuprincipal" depuis le .fxml
	 */
	public MenuPrincipalController() {
		// On construit la stage
		stage = new Stage();

		try {
			// récupération du FXML (scenebuilder)
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/menuPrincipal.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.getIcons()
					.add(new Image(getClass().getResourceAsStream("/fr/locacom/view/ressources/images/logo.png")));
			stage.setTitle("LocaCom");
			stage.setMaximized(true);
			stage.setMinHeight(600);
			stage.setMinWidth(800);
			stage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				/* si on appuie szur ECHAP */
				if (event.getCode() == KeyCode.ESCAPE) {
					/* et on qu'est entrain d'ajouter sur location */
					if (AjoutLocationController.addLocation) {
						/* On annule l'ajout */
						AjoutLocationController.addLocation = false;
						/* on informe */
						Alert alert = new Alert(Alert.AlertType.INFORMATION,
								"L'ajout de la location a bien été annulé");
						alert.show();
					}
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// on recupere d'abord les clients
		ListeClients.recupererClients();
		ListeLocations.recupererLocations();
		// on recupère les panneaux
		ListePanneaux.recupererPanneaux();
		/* Quand on click sur la carte */
		map.setOnMouseClicked(e -> {
			/* Si on est dans la procedure d'ajout d'un panneau */
			if (AjoutPanneauController.isAddSign()) {

				/* On empeche l'ajout d'un 2nd panneau a la suite */
				AjoutPanneauController.setAddSign(false);
				/* On récupère le panneau en cours d'ajout */
				Panneau p = AjoutPanneauController.getPanneau();
				/* On associe le bouton */
				associerBouton(e, p);
				/* On l'ajoute dans la liste */
				ListePanneauxControler.getPanneaux().add(p);
			}
		});
		/* En fin d'initalisation > On place les panneaux */
		placerPanneaux();

	}

	/**
	 * @param e l'event de la souris
	 * @param p le panneau dont il faut associé le bouton
	 * 
	 *          Permet d'associer un bouton pour représenter le panneau sur la map a
	 *          un panneau
	 */
	private void associerBouton(MouseEvent e, Panneau p) {

		/** Creation d'un nouveau bouton avec l'id du panneau */
		Button b = new Button(String.valueOf(p.getId()));
		/* On ajoute l'evenement du click a ce bouton */
		b.setOnMouseClicked(event_1 -> {

			/**
			 * Si on est dans la procedure d'ajout d'une location et que le panneau peut se
			 * louer
			 */
			if (AjoutLocationController.addLocation && p.isReservable()) {

				/*
				 * quand on click on va ajouter une location du panneau selectionner On
				 * renseigne le panneau a louer
				 */
				AjoutLocationController.panneau = p;
				/* On ouvre la fenetre pour poursuivre la location */
				Principal.ouvrirFenetreAjoutLocation();
				/** Sinon si on est dans la procedure de suppresion d'une location */
			} else if (supprimerPanneau) {
				/* quand on click on va gerer la suppresion de ce panneau */
				ListePanneauxControler.gererDelete(p);

			}
			/* Sinon si le panneau est complet */
			else if (!p.isReservable()) {
				Alert alert = new Alert(AlertType.ERROR, "Ce panneau est complet");
				alert.show();
				AjoutLocationController.addLocation = false;
			}
		});
		/* Si ce n'est pas un replacage panneau éxistant */
		if (e != null) {
			b.setStyle("-fx-background-color: green;-fx-text-fill: white;");

			b.setLayoutX(e.getSceneX());
			b.setLayoutY(e.getSceneY()-80);
			/* On enregistre les coordonnees dans la panneaux */
			p.setX(b.getLayoutX());
			p.setY(b.getLayoutY());

		} else {
			/** Couleur du panneau selon le nombre de location */
			if (p.getLocations().size() == 1) {
				b.setStyle("-fx-background-color: blue;-fx-text-fill: white;");
			} else if (p.getLocations().size() == 2) {
				b.setStyle("-fx-background-color: orange;-fx-text-fill: white;");
			} else if (p.getLocations().size() == 3) {
				b.setStyle("-fx-background-color: red;-fx-text-fill: white;");
			} else {
				b.setStyle("-fx-background-color: green;-fx-text-fill: white;");
			}
			b.setLayoutX(p.getX());
			b.setLayoutY(p.getY());
		}
		/** Evenement du survol du bouton */
		b.setOnMouseEntered(event_2 -> {

			/* Si on est pas entrain d'ajouter une location OU de supprimer un panneau */
			if (!AjoutLocationController.addLocation && !supprimerPanneau) {
				/* on calcule la position de la fenetre hover */
				FenPanneau f = calculHover(p);
				/* on l'ajoute sur la carte */
				pane_map.getChildren().add(f.getCadre());
				/* Evenement lorsque l'on quitte cette fenetre il faut la supprimer */
				f.getCadre().setOnMouseExited(event_3 -> {
					/* Si le panneau éxiste toujours évite un bug de suppresion */
					if (!AjoutLocationController.addLocation && ListePanneaux.retournePanneau(p.getId()) != null) {
						/* On le supprime de la carte */
						pane_map.getChildren().remove(f.getCadre());
					}
					//
				});
			}
		});
		/* On l'ajoute le bouton au panneau */
		p.setBouton(b);
		/* On le place sur la map */
		pane_map.getChildren().add(b);

	}

	/**
	 * @param p le panneau dont il faut calculé
	 * @return Le panneau avec les bonnes coordonnées et un affichage totale
	 * 
	 *         Algorithme calcul de la position du panneau selon la taille et
	 *         l'espace libre
	 */
	private FenPanneau calculHover(Panneau p) {
		// TAILLE fenetre du panneau
		final double PAN_WIDTH = 300;
		final double PAN_HEIGHT = 380;
		// TAILLE de la fenetre generale
		final double STAGE_WIDTH = stage.getWidth();
		final double STAGE_HEIGHT = stage.getHeight();
		// COORDONNES du bouton (panneau)
		final double PAN_X = p.getX();
		final double PAN_Y = p.getY();
		// Coefficient a ajoute pour adapter

		double coef_x = 0;
		double coef_y = 0;

		// si on dépasse sur X -> on calcule le coef a ajouter
		if (PAN_X + PAN_WIDTH > STAGE_WIDTH) {
			coef_x = (STAGE_WIDTH - (PAN_X + PAN_WIDTH));
		}
		// si on dépasse sur Y -> on calcule le coef a ajouter
		if (PAN_Y + PAN_HEIGHT > STAGE_HEIGHT) {
			coef_y = (STAGE_HEIGHT - (PAN_Y + PAN_HEIGHT));

		}
		// on cree la fenetre en tenant compte de ces coefs
		FenPanneau f = new FenPanneau(p, PAN_X + coef_x, PAN_Y + coef_y);
		return f;
	}

	/**
	 * Place tous les panneaux enregistres sur la carte
	 */
	public void placerPanneaux() {
		/* On clear la carte */
		pane_map.getChildren().remove(1, pane_map.getChildren().size());
		/* Pour tous les panneaux */
		for (Panneau p : ListePanneaux.getListePanneaux()) {
			/* on associe un bouton avec l'event NULL car il s'agit d'une replacage */
			associerBouton(null, p);
		}

	}
	/**
	 * @param e Action du bouton aide
	 */
	@FXML
	private void help(ActionEvent e) {
		
		System.out.println("Ouverture fenêtre d'aide");
		Principal.ouvrirAide();
	}
	/**
	 * @param e Action du bouton fermer
	 */
	@FXML
	private void close(ActionEvent e) {
		this.stage.close();
	}
	/**
	 * @param e Action du bouton enregistre
	 */
	@FXML
	private void save(ActionEvent e) {
		ListePanneaux.enregistrerPanneaux();
		ListeClients.enregistrerClients();
		ListeLocations.enregistrerLocations();
	}

	/**
	 * @param e Ouvre la fenetre liste des clients
	 */
	@FXML
	private void listeClients(ActionEvent e) {

		Principal.ouvrirFenetreListeClients();

	}

	/**
	 * @param e Ouvre la fenetre liste des panneaux
	 */
	@FXML
	private void listePanneaux(ActionEvent e) {

		Principal.ouvrirFenetreListePanneaux();

	}

	/**
	 * @param e Ouvre la fenetre liste des locations
	 */
	@FXML
	private void listeLocations(ActionEvent e) {

		Principal.ouvrirFenetreListeLocations();

	}

	/**
	 * @param e Ouvre la fenetre d'ajout d'un panneau
	 */
	@FXML
	private void ajoutPanneau(ActionEvent e) {

		Principal.ouvrirFenetreAjoutPanneau();

	}

	/**
	 * @param e Démarre la procédure d'ajout d'une location
	 */
	@FXML
	private void ajoutLocation(ActionEvent e) {
		// Si il éxiste des panneaux
		if (!ListePanneaux.getListePanneaux().isEmpty()) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Location panneau");
			alert.setHeaderText("Cliquer sur un panneau pour ajouter une location");
			alert.setContentText("Appuyez sur 'ECHAP' pour annuler");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				// On d�marre la proc�dure d'ajout d'une location
				AjoutLocationController.addLocation = true;
			} else {

			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Location");
			alert.setHeaderText("Erreur : pas de panneaux");
			alert.setContentText("Veuillez ajouter un panneau pour lui ajouter une location");
			alert.showAndWait();
		}

	}

	/**
	 * @param e Ouvre la fenetre d'ajout d'un client
	 */
	@FXML
	private void ajoutClient(ActionEvent e) {

		Principal.ouvrirFenetreAjoutClient();

	}

	@FXML
	private void supprimerClient(ActionEvent e) {

		Principal.ouvrirFenetreListeClients();

	}

	@FXML
	private void supprimerLocation(ActionEvent e) {

		Principal.ouvrirFenetreListeLocations();

	}

	/**
	 * @param e gère la suppression d'un panneau depuis la carte
	 */
	@FXML
	private void supprimerPanneau(ActionEvent e) {

		if (!ListePanneaux.getListePanneaux().isEmpty()) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Suppression panneau");
			alert.setHeaderText("");
			alert.setContentText("Cliquer sur un panneau pour le supprimer");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				supprimerPanneau = true;
			} else {
				supprimerPanneau = false;
			}

		}
	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the root
	 */
	public VBox getRoot() {
		return root;
	}

	/**
	 * @param root the root to set
	 */
	public void setRoot(VBox root) {
		this.root = root;
	}

	/**
	 * @return the b1
	 */
	public Button getB1() {
		return b1;
	}

	/**
	 * @param b1 the b1 to set
	 */
	public void setB1(Button b1) {
		this.b1 = b1;
	}

	/**
	 * @return the listerClient
	 */
	public Button getListerClient() {
		return listerClient;
	}

	/**
	 * @param listerClient the listerClient to set
	 */
	public void setListerClient(Button listerClient) {
		this.listerClient = listerClient;
	}

	/**
	 * @return the map
	 */
	public ImageView getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(ImageView map) {
		this.map = map;
	}

	/**
	 * @return the pane_map
	 */
	public Pane getPane_map() {
		return pane_map;
	}

	/**
	 * @param pane_map the pane_map to set
	 */
	public void setPane_map(Pane pane_map) {
		this.pane_map = pane_map;
	}

	/**
	 * @return the supprimerPanneau
	 */
	public static boolean isSupprimerPanneau() {
		return supprimerPanneau;
	}

	/**
	 * @param supprimerPanneau the supprimerPanneau to set
	 */
	public static void setSupprimerPanneau(boolean supprimerPanneau) {
		MenuPrincipalController.supprimerPanneau = supprimerPanneau;
	}

}
