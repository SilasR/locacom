package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import fr.locacom.main.Principal;
import fr.locacom.model.Client;

/** Controler de la fenêtre d'ajout client */
public class AjoutClientController implements Initializable {

	/** stage fenêtre ajout client */
	private Stage stage;
	@FXML
	private Button valider;
	@FXML
	private Button bnAnnuler;
	@FXML
	private TextField adresse;
	@FXML
	private TextField telephone;
	@FXML
	private TextField nom;

	@FXML
	private Label info;

	/**
	 * Constructeur qui charge la scene "ajouterClient" depuis le .fxml
	 */
	public AjoutClientController() {
		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/ajoutClient.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - ajout clients");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			/* Evenements du claviers */
			stage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				/* si on appuie szur ENTREE */
				if (event.getCode() == KeyCode.ENTER) {
					/* et que les champs sont OK */
					if (!valider.isDisabled()) {
						ajoutClient();
						event.consume();
					}
				}
			});



		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode pour ajouter un client
	 */
	private void ajoutClient() {
		if(telephone.getText().isEmpty()) {
			new Client(nom.getText(), adresse.getText());		
		}else {
			new Client(nom.getText(), adresse.getText(),telephone.getText());			
		}

		ListeClientsController.refresh();
		Principal.fermerFenetreAjoutClient();

	}

	

	@Override
	public void initialize(URL location, ResourceBundle resources) {


		/* On désative le bouton valider si les champs sont vides */
		valider.disableProperty()
				.bind(Bindings.isEmpty(nom.textProperty()).or(Bindings.isEmpty(adresse.textProperty())));

		bnAnnuler.setOnAction(e -> stage.close());

		valider.setOnAction(e -> ajoutClient());
	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the valider
	 */
	public Button getValider() {
		return valider;
	}

	/**
	 * @param valider the valider to set
	 */
	public void setValider(Button valider) {
		this.valider = valider;
	}

	/**
	 * @return the bnAnnuler
	 */
	public Button getBnAnnuler() {
		return bnAnnuler;
	}

	/**
	 * @param bnAnnuler the bnAnnuler to set
	 */
	public void setBnAnnuler(Button bnAnnuler) {
		this.bnAnnuler = bnAnnuler;
	}

	/**
	 * @return the adresse
	 */
	public TextField getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(TextField adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the telephone
	 */
	public TextField getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(TextField telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the nom
	 */
	public TextField getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(TextField nom) {
		this.nom = nom;
	}

	/**
	 * @return the info
	 */
	public Label getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(Label info) {
		this.info = info;
	}

}
