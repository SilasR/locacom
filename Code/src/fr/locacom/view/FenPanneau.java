package fr.locacom.view;

import fr.locacom.controller.ListePanneauxControler;
import fr.locacom.model.Panneau;
import fr.locacom.model.Visibilite;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class FenPanneau {

	private GridPane cadre = new GridPane();
	private Label tarif;
	private Label visibilite;
	private Label taille;
	private Label id;
	private Label nbLocations;
	private Label details;
	private Label tarifl;
	private Label visibilitel;
	private Label taillel;
	private Label idl;
	private Label nbLocationsl;
	private Label detailsl;
	private Button supprimer = new Button("supprimer");
	private Button modifier = new Button("modifier");
	private Visibilite vis;

	public FenPanneau(Panneau p, Double x, Double y) {
		/*
		 * Scene scene = null; scene = new Scene(creerContenu(p));
		 * this.setScene(scene); this.setTitle("Panneau n�" + p.getId());
		 * this.sizeToScene(); this.setResizable(false);
		 */
		creerContenu(p, x, y);
	}

	Parent creerContenu(Panneau p, Double x, Double y) {
		// cr�er un gridPane pour mettre tous les infos du panneau dedans

		// cr�er les labels des informations sur le panneau
		this.tarifl = new Label("TARIF :");
		this.tarif = new Label(String.valueOf(p.getTarif())+" € / J");
		this.visibilitel = new Label("VISIBILITE :");
		this.vis = p.getVisibilite();
		this.visibilite = new Label(this.vis.getNom());
		this.taillel = new Label("TAILLE :");
		this.taille = new Label(String.valueOf(p.getLargeur() + "x" + p.getLongueur() + "m"));
		this.idl = new Label("IDENTIFIANT :");
		this.id = new Label(String.valueOf(p.getId()));
		this.nbLocationsl = new Label("NB LOCATIONS :");
		this.nbLocations = new Label(String.valueOf(p.getLocations().size()));
		this.detailsl = new Label("DETAILS :");
		this.details = new Label(String.valueOf(p.getDetails()));

		// creer le style des boutons
		modifier.setStyle("-fx-font: 13 arial; -fx-base: #42f4ee;");
		supprimer.setStyle("-fx-font: 13 arial; -fx-base: #f44147;");
		modifier.setOnAction(e -> {
			ListePanneauxControler.gererEdit(p);
		});
		supprimer.setOnAction(e -> {
			ListePanneauxControler.gererDelete(p);
		});

		// ajouter les labels & boutons dans le GridPane
		cadre.add(tarifl, 0, 0);
		cadre.add(tarif, 1, 0);
		cadre.add(visibilitel, 0, 1);
		cadre.add(visibilite, 1, 1);
		cadre.add(taillel, 0, 2);
		cadre.add(taille, 1, 2);
		cadre.add(idl, 0, 3);
		cadre.add(id, 1, 3);
		cadre.add(nbLocationsl, 0, 4);
		cadre.add(nbLocations, 1, 4);
		cadre.add(detailsl, 0, 5);
		cadre.add(details, 1, 5);
		cadre.add(modifier, 0, 6);
		cadre.add(supprimer, 1, 6);

		// creer le style general de la page
		cadre.setPadding(new Insets(25, 25, 25, 25));
		cadre.setHgap(30);
		cadre.setVgap(20);

		// placer horizontalement � gauche des labels
		GridPane.setHalignment(this.tarif, HPos.CENTER);
		GridPane.setHalignment(this.visibilite, HPos.CENTER);
		GridPane.setHalignment(this.taille, HPos.CENTER);
		GridPane.setHalignment(this.id, HPos.CENTER);
		GridPane.setHalignment(this.nbLocations, HPos.CENTER);
		GridPane.setHalignment(this.details, HPos.CENTER);
		GridPane.setHalignment(this.tarifl, HPos.LEFT);
		GridPane.setHalignment(this.visibilitel, HPos.LEFT);
		GridPane.setHalignment(this.taillel, HPos.LEFT);
		GridPane.setHalignment(this.idl, HPos.LEFT);
		GridPane.setHalignment(this.nbLocationsl, HPos.LEFT);
		GridPane.setHalignment(this.detailsl, HPos.LEFT);
		// centrer verticalement des labels
		GridPane.setValignment(tarif, VPos.CENTER);
		GridPane.setValignment(visibilite, VPos.CENTER);
		GridPane.setValignment(taille, VPos.CENTER);
		GridPane.setValignment(id, VPos.CENTER);
		GridPane.setValignment(nbLocations, VPos.CENTER);
		GridPane.setValignment(details, VPos.CENTER);
		GridPane.setValignment(tarifl, VPos.CENTER);
		GridPane.setValignment(visibilitel, VPos.CENTER);
		GridPane.setValignment(taillel, VPos.CENTER);
		GridPane.setValignment(idl, VPos.CENTER);
		GridPane.setValignment(nbLocationsl, VPos.CENTER);
		GridPane.setValignment(detailsl, VPos.CENTER);

		// placer les boutons
		GridPane.setHalignment(modifier, HPos.LEFT);
		GridPane.setValignment(modifier, VPos.CENTER);
		GridPane.setHalignment(supprimer, HPos.LEFT);
		GridPane.setValignment(supprimer, VPos.CENTER);

		cadre.setPrefSize(260, 300);

		cadre.setLayoutX(x);
		cadre.setLayoutY(y);
		cadre.setStyle("-fx-background-color:#badbc0; -fx-opacity:1;");
		return cadre;
	}

	/**
	 * @return the groot
	 */
	public GridPane getCadre() {
		return cadre;
	}

	/**
	 * @param groot
	 *            the groot to set
	 */
	public void setCadre(GridPane cadre) {
		this.cadre = cadre;
	}

}
