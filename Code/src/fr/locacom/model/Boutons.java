package fr.locacom.model;

import javafx.scene.control.Button;

/**
 * Classe permettant d'ajouter des boutons d'edition et de suppresion dans les
 * listes de clients, locations et panneaux
 */
public abstract class Boutons {

	private Button edit;
	private Button delete;


	public Boutons() {
		this.edit = new Button("Modifier");
		this.delete = new Button("Supprimer");
		this.edit.getStyleClass().add("button_modifier");
		this.delete.getStyleClass().add("button_supprimer");
	}

	/**
	 * @return the edit
	 */
	public Button getEdit() {
		return edit;
	}

	/**
	 * @param edit the edit to set
	 */
	public void setEdit(Button edit) {
		this.edit = edit;
	}

	/**
	 * @return the delete
	 */
	public Button getDelete() {
		return delete;
	}

	/**
	 * @param delete the delete to set
	 */
	public void setDelete(Button delete) {
		this.delete = delete;
	}

}
