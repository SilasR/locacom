package fr.locacom.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import fr.locacom.controller.AjoutPanneauController;
import fr.locacom.controller.ListePanneauxControler;
import fr.locacom.main.Principal;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;

/**
 * Classe permettant la creation d'un panneau avec un tarif,des details, une
 * Visibilite, une longueur, une largeur et liste de locations
 */
public class Panneau extends Boutons {
	/* Compteur du nombre de panneau */
	public static int nbrpan = 0;
	/* Tarif du panneau */
	private double tarif;
	/* Visibilite du panneau */
	private Visibilite visibilite;
	/* Longueur du panneau */
	private int longueur;
	/* Largeur du panneau */
	private int largeur;
	/* ID du panneau attribué automatiquement et unique */
	private int id;
	/* Liste de locations du panneau */
	private ArrayList<Location> locations;
	/* Details du panneau */
	private String details;
	/* Bouton > correspond a la representation graphique du panneau sur la carte */
	private Button bouton;
	/* coordonnees X du panneau */
	private double x;
	/* coordonnees Y du panneau */
	private double y;

	/**
	 * @param t
	 * @param visi
	 * @param longeur
	 * @param largeur
	 * @param d
	 * 
	 *                Construit un panneau
	 */
	public Panneau(Double t, Visibilite visi, int longeur, int largeur, String details) {

		this.tarif = t;
		this.visibilite = visi;
		this.longueur = longeur;
		this.largeur = largeur;
		this.details = details;
		nbrpan++;
		this.id = nbrpan;
		this.locations = new ArrayList<Location>();

	}

	/**
	 * @param t
	 * @param visi
	 * @param longeur
	 * @param largeur
	 * @param id
	 * @param listeLoca
	 * @param details
	 * @param x
	 * @param y
	 * 
	 *                  Construit un panneau enregistre dans un fichier donc on
	 *                  connait deja tout (id listeLoca, id)
	 */
	public Panneau(Double t, Visibilite visi, int longeur, int largeur, int id, ArrayList<Location> listeLoca,
			String details, Double x, Double y) {
		this.tarif = t;
		this.visibilite = visi;
		this.longueur = longeur;
		this.largeur = largeur;
		this.id = id;
		this.details = details;
		this.locations = listeLoca;
		this.x = x;
		this.y = y;
		super.getEdit().setOnAction(e -> {
			ListePanneauxControler.gererEdit(this);
		});
		super.getDelete().setOnAction(e -> {
			ListePanneauxControler.gererDelete(this);
		});

	}


	

	/**
	 * @param t
	 * @param visi
	 * @param taille
	 * @param details
	 * 
	 *                Construit un panneau a partir de rien
	 */
	public Panneau(Double t, Visibilite visi, int taille, String details) {

		super();
		this.tarif = t;
		this.visibilite = visi;
		this.longueur = taille;
		this.largeur = taille;
		this.details = details;
		nbrpan++;
		this.id = nbrpan;
		this.locations = new ArrayList<Location>();

	}

	/**
	 * @param p
	 * @return true si on peut reserver ce panneau
	 */
	public boolean isReservable() {

		return (this.locations.size() < 3);

	}

	/**
	 * @param l
	 * 
	 *          Methode pour ajouter une location a ce panneau
	 */
	public void ajouterLocation(Location l) {
		if (l != null) {
			// Si on on ne d�passe pas trois locations
			if (this.isReservable()) {
				this.locations.add(l);
				l.ajouterPanneau(this);
			} else {
				System.out.println("erreur : ce panneau est complet");
			}
		}
	}

	/**
	 * @param l
	 * 
	 *          Methode pour supprimer une location d'un panneau
	 */
	public void supprimerLocation(int id) {
		Iterator<Location> itr = this.locations.iterator();
		while (itr.hasNext()) {
			Location l = itr.next();
			if (l.getIdLoca() == id) {
				itr.remove();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Panneau " + id;
	}

	/**
	 * @param tarif
	 * @param visi
	 * @param longueur
	 * @param largeur
	 * @param details
	 * @return
	 * 
	 *         Permet d'ajouter un nouveau panneau en effectuant toutes les
	 *         vérification nécéssaires
	 */
	public static boolean ajouterPanneau(String tarif, Visibilite visi, int longueur, int largeur, String details) {
		// variable pour savoir si tout s'est bien passé
		boolean success = false;
		boolean numeric = true;
		Double tarif_d = null;
		// On regarde si le tarif saisie est correcte (si c'est un nombre)
		try {
			tarif_d = Double.parseDouble(tarif);
		} catch (NumberFormatException e) {
			numeric = false;
		}
		// Si toutes les conditions sont respectés
		if (numeric && longueur != 0 && largeur != 0 && details != null) {
			AjoutPanneauController.getStage().close();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Nouveau panneau");
			alert.setHeaderText("Cliquer sur la carte pour placer le panneau");
			alert.setContentText("Appuyez sur 'ECHAP' pour annuler");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Panneau p;
				// si le panneau est un carré
				if (longueur == largeur) {
					p = new Panneau(tarif_d, visi, largeur, details);
				} else {
					p = new Panneau(tarif_d, visi, largeur, longueur, details);
				}

				AjoutPanneauController.setPanneau(p);
				// On ferme la fenêtre d'ajout
				Principal.fermerFenetreAjoutPanneau();
				// On autorise le placement sur la carte
				AjoutPanneauController.setAddSign(true);
				// on l'ajout a la liste
				ListePanneaux.getListePanneaux().add(p);
				// tout est ok
				success = true;
			}

		}
		return success;
	}

	/**
	 * @return the nbrpan
	 */
	public static int getNbrpan() {
		return nbrpan;
	}

	/**
	 * @param nbrpan the nbrpan to set
	 */
	public static void setNbrpan(int nbrpan) {
		Panneau.nbrpan = nbrpan;
	}

	/**
	 * @return the tarif
	 */
	public double getTarif() {
		return tarif;
	}

	/**
	 * @param tarif the tarif to set
	 */
	public void setTarif(double tarif) {
		this.tarif = tarif;
	}

	/**
	 * @return the visibilite
	 */
	public Visibilite getVisibilite() {
		return visibilite;
	}

	/**
	 * @param visibilite the visibilite to set
	 */
	public void setVisibilite(Visibilite visibilite) {
		this.visibilite = visibilite;
	}

	/**
	 * @return the longueur
	 */
	public int getLongueur() {
		return longueur;
	}

	/**
	 * @param longueur the longueur to set
	 */
	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	/**
	 * @return the largeur
	 */
	public int getLargeur() {
		return largeur;
	}

	/**
	 * @param largeur the largeur to set
	 */
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the locations
	 */
	public ArrayList<Location> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @return the bouton
	 */
	public Button getBouton() {
		return bouton;
	}

	/**
	 * @param bouton the bouton to set
	 */
	public void setBouton(Button bouton) {
		this.bouton = bouton;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

}
