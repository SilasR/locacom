package fr.locacom.model;

import fr.locacom.controller.ListeLocationsController;

/**
 * Classe permettant la creation d'une location avec un Client, un Panneau, une
 * date de debut et une date de fin
 */
public class Location extends Boutons {

	/* Compteur de location pour numero de location unique */
	public static int cpt = 0;
	/* Duree de la location */
	private int duree;
	/* Client qui a la location */
	private Client client;
	/* panneau qui est loue */
	private Panneau panneau;
	/* ID de la location > attribue automatiquement et unique */
	private int idLoca;
	/* date de debut de la location */
	private Date dateDebut;
	/* date de fin de la location */
	private Date dateFin;

	/**
	 * @param duree
	 * @param client
	 * @param panneau
	 * @param idLoca
	 * @param dateDebut
	 * @param dateFin
	 * 
	 *                  Construit une nouvelle location
	 */
	public Location(int duree, Client client, Panneau panneau, Date dateDebut, Date dateFin) {
		super();
		this.duree = duree;
		this.client = client;
		/* On augmente le nombre de réservation du client */
		client.setNbResa(client.getNbResa() + 1);
		/* verif a faire pour le client */
		cpt++;
		this.idLoca = cpt;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.panneau = panneau;
		/*
		 * méthode pour ajouter une location vérification nombre de location et panneau
		 * OK
		 */

		// this.panneau.ajouterLocation(this);

	}


	/**
	 * @param id
	 * @param client
	 * @param panneau
	 * @param dateDebut
	 * @param dateFin
	 * @param edit
	 * @param delete
	 * 
	 *                  Construit une location dont on connait deja l'ID 
	 */

	public Location(int id, Client client, Panneau panneau, Date dateDebut, Date dateFin,int duree) {
		super();
		this.idLoca = id;
		this.client = client;
		this.panneau = panneau;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.duree = duree;

		// on associe les actions aux deux boutons
		super.getEdit().setOnAction(e -> {
			ListeLocationsController.gererEdit(this);
		});
		super.getDelete().setOnAction(e -> {
			ListeLocationsController.gererDelete(this);
		});


	}

	/**
	 * @param p
	 * 
	 *          Methode pour ajouter un panneau a la location
	 */
	public void ajouterPanneau(Panneau p) {

		this.panneau = p;
	}

	/**
	 * @param p
	 * 
	 *          Methode pour supprimer un panneau d'une location
	 */
	public void supprimerPanneau(Panneau p) {

		this.panneau = null;
	}

	/**
	 * @return the duree
	 */
	public int getDuree() {
		return duree;
	}

	/**
	 * @param duree the duree to set
	 */
	public void setDuree(int duree) {
		this.duree = duree;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * @return the panneau
	 */
	public Panneau getPanneau() {
		return panneau;
	}

	/**
	 * @param panneau the panneau to set
	 */
	public void setPanneau(Panneau panneau) {
		this.panneau = panneau;
	}

	/**
	 * @return the cpt
	 */
	public static int getCpt() {
		return cpt;
	}

	/**
	 * @param cpt the cpt to set
	 */
	public static void setCpt(int cpt) {
		Location.cpt = cpt;
	}

	/**
	 * @return the idLoca
	 */
	public int getIdLoca() {
		return idLoca;
	}

	/**
	 * @param idLoca the idLoca to set
	 */
	public void setIdLoca(int idLoca) {
		this.idLoca = idLoca;
	}

	/**
	 * @return the dateDebut
	 */
	public Date getDateDebut() {
		return dateDebut;
	}

	/**
	 * @param dateDebut the dateDebut to set
	 */
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	/**
	 * @return the dateFin
	 */
	public Date getDateFin() {
		return dateFin;
	}

	/**
	 * @param dateFin the dateFin to set
	 */
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

}
