package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import fr.locacom.main.Principal;
import fr.locacom.model.Client;
import fr.locacom.model.Date;
import fr.locacom.model.ListeLocations;
import fr.locacom.model.ListePanneaux;
import fr.locacom.model.Location;
import fr.locacom.model.Panneau;

/** Controler de la fenetre liste locations */
public class ListeLocationsController implements Initializable {

	/** Stage de liste locations */
	private Stage stage;
	/**
	 * Controller du menu principal pour avoir accès aux méthodes pour supprimer les
	 * panneaux
	 */
	private static MenuPrincipalController menu;
    @FXML
    private TableView<Location> tableLocations;

    @FXML
    private TableColumn<Location, Integer> id;

    @FXML
    private TableColumn<Location, Client> client;

    @FXML
    private TableColumn<Location, Panneau> panneau;

    @FXML
    private TableColumn<Location, Date> date_debut;

    @FXML
    private TableColumn<Location, Date> date_fin;

    @FXML
    private TableColumn<Location, Button> edit;

    @FXML
    private TableColumn<Location, Button> delete;
    @FXML
    private TableColumn<Location, Integer> duree;
    @FXML
    private Button bnAjoutLocation;

    @FXML
    private Button bnClose;
    @FXML
    private Button bnDelete;

	private static ObservableList<Location> locations = FXCollections.observableArrayList();

	/**
	 * Constructeur qui charge la scene "listeLocations" depuis le .fxml
	 */
	public ListeLocationsController(MenuPrincipalController mc) {
		menu = mc;
		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/listeLocations.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - liste locations");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		refresh();

		id.setCellValueFactory(new PropertyValueFactory<>("idLoca"));
		client.setCellValueFactory(new PropertyValueFactory<>("client"));
		panneau.setCellValueFactory(new PropertyValueFactory<>("panneau"));
		date_debut.setCellValueFactory(new PropertyValueFactory<>("dateDebut"));
		date_fin.setCellValueFactory(new PropertyValueFactory<>("dateFin"));	
		edit.setCellValueFactory(new PropertyValueFactory<>("edit"));
		delete.setCellValueFactory(new PropertyValueFactory<>("delete"));
		duree.setCellValueFactory(new PropertyValueFactory<>("duree"));
		
		tableLocations.setItems(getLocationsList());
		bnAjoutLocation.setOnAction(event -> {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Location panneau");
			alert.setHeaderText("");
			alert.setContentText("Cliquer sur un panneau pour ajouter une location");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				Principal.fermerFenetreListeLocations();
				// On démarre la procédure d'ajout d'une location
				AjoutLocationController.addLocation = true;
			}

		});
		bnClose.setOnAction(e -> {

			stage.close();
		});
		bnDelete.setDisable(true);
		tableLocations.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tableLocations.setOnMouseClicked(e->{
			//Si on a selectionné plus de 1 locations
			if(tableLocations.getSelectionModel().getSelectedItems().size() > 1) {
				bnDelete.setDisable(false);
			}else {
				bnDelete.setDisable(true);				
			}
		});
		bnDelete.setOnAction(e->{
			for(Location l : tableLocations.getSelectionModel().getSelectedItems()) {
				gererDelete(l);
			}
			bnDelete.setDisable(true);
		});
	}

	/**
	 * Méthode pour raffraichir la liste des locations
	 */
	public static void refresh(){

		locations.clear();
		for (Location l : ListeLocations.getListeLocations()) {
			locations.add(new Location(l.getIdLoca(), l.getClient(), l.getPanneau(), l.getDateDebut(),l.getDateFin(),l.getDuree()));
		}

	}

	/**
	 * @return liste des locations
	 */
	public static ObservableList<Location> getLocationsList() {

		return locations;
	}

	/**
	 * Permet de gérer l'edition d'une location
	 * 
	 * @param l la location a edité
	 */
	public static void gererEdit(Location l) {
		Principal.ouvrirFenetreModifLocation(l);
	}

	/**
	 * Permet de gérer la suppresion d'une location
	 * 
	 * @param l la location a supprimé
	 */
	public static void gererDelete(Location l) {
		/* On demande confirmation */
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation");
		alert.setHeaderText("");
		alert.setContentText("Etes vous sur de vouloir supprimer cette location "+l.getIdLoca());
		Optional<ButtonType> result = alert.showAndWait();
		/* on regarde sa réponse */
		if (result.get() == ButtonType.OK) {
			/* On supprime de l'obserable list */
			locations.remove(l);
			/* On recupère la location */
			Location loc = ListeLocations.retourneLocation(l.getIdLoca());
			/* On recupère le panneau */
			Panneau p = ListePanneaux.retournePanneau(loc.getPanneau().getId());
			/* On enlève la location du panneau */
			p.supprimerLocation(loc.getIdLoca());
			/* On le supprime dans les fichiers */
			ListeLocations.supprimerLocation(loc.getIdLoca());
			/* On replace les panneaux */
			menu.placerPanneaux();

		}
	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the tableLocations
	 */
	public TableView<Location> getTableLocations() {
		return tableLocations;
	}

	/**
	 * @param tableLocations the tableLocations to set
	 */
	public void setTableLocations(TableView<Location> tableLocations) {
		this.tableLocations = tableLocations;
	}

	/**
	 * @return the id
	 */
	public TableColumn<Location, Integer> getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(TableColumn<Location, Integer> id) {
		this.id = id;
	}

	/**
	 * @return the client
	 */
	public TableColumn<Location, Client> getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(TableColumn<Location, Client> client) {
		this.client = client;
	}

	/**
	 * @return the panneau
	 */
	public TableColumn<Location, Panneau> getPanneau() {
		return panneau;
	}

	/**
	 * @param panneau the panneau to set
	 */
	public void setPanneau(TableColumn<Location, Panneau> panneau) {
		this.panneau = panneau;
	}

	/**
	 * @return the date_debut
	 */
	public TableColumn<Location, Date> getDate_debut() {
		return date_debut;
	}

	/**
	 * @param date_debut the date_debut to set
	 */
	public void setDate_debut(TableColumn<Location, Date> date_debut) {
		this.date_debut = date_debut;
	}

	/**
	 * @return the date_fin
	 */
	public TableColumn<Location, Date> getDate_fin() {
		return date_fin;
	}

	/**
	 * @param date_fin the date_fin to set
	 */
	public void setDate_fin(TableColumn<Location, Date> date_fin) {
		this.date_fin = date_fin;
	}

	/**
	 * @return the edit
	 */
	public TableColumn<Location, Button> getEdit() {
		return edit;
	}

	/**
	 * @param edit the edit to set
	 */
	public void setEdit(TableColumn<Location, Button> edit) {
		this.edit = edit;
	}

	/**
	 * @return the delete
	 */
	public TableColumn<Location, Button> getDelete() {
		return delete;
	}

	/**
	 * @param delete the delete to set
	 */
	public void setDelete(TableColumn<Location, Button> delete) {
		this.delete = delete;
	}

	/**
	 * @return the bnAjoutLocation
	 */
	public Button getBnAjoutLocation() {
		return bnAjoutLocation;
	}

	/**
	 * @param bnAjoutLocation the bnAjoutLocation to set
	 */
	public void setBnAjoutLocation(Button bnAjoutLocation) {
		this.bnAjoutLocation = bnAjoutLocation;
	}

	/**
	 * @return the bnClose
	 */
	public Button getBnClose() {
		return bnClose;
	}

	/**
	 * @param bnClose the bnClose to set
	 */
	public void setBnClose(Button bnClose) {
		this.bnClose = bnClose;
	}

	/**
	 * @return the bnDelete
	 */
	public Button getBnDelete() {
		return bnDelete;
	}

	/**
	 * @param bnDelete the bnDelete to set
	 */
	public void setBnDelete(Button bnDelete) {
		this.bnDelete = bnDelete;
	}

	/**
	 * @return the locations
	 */
	public static ObservableList<Location> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public static void setLocations(ObservableList<Location> locations) {
		ListeLocationsController.locations = locations;
	}

}
