package fr.locacom.controller;

import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;
import fr.locacom.model.Panneau;
import fr.locacom.model.Visibilite;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/** Controler de la fenêtre d'ajout d'un panneau */
public class AjoutPanneauController implements Initializable {

	/** Stage fenêtre ajout panneau */
	private static Stage stage;
	/**
	 * variable pour savoir si on est dans la procédure d'ajout d'un panneau dans ce
	 * cas le click sur la carte engendre le placement
	 */
	private static boolean addSign = false;
	/*
	 * Contient le panneau a ajouter
	 */
	private static Panneau panneau = null;
	@FXML
	private RadioButton format_rectangle;

	@FXML
	private ToggleGroup format;

	@FXML
	private RadioButton format_square;

	@FXML
	private Slider hauteurSlider;

	@FXML
	private Label hauteur_label;

	@FXML
	private Slider longueurSlider;

	@FXML
	private Label longueur_label;

	@FXML
	private TextField details;
	@FXML
	private TextField prix;
	@FXML
	private ChoiceBox<Visibilite> visi;

	@FXML
	private Button valider;

	@FXML
	private Button bnAnnuler;
	@FXML
	private Label info;

	/**
	 * Constructeur qui charge la scene "ajouterpanneau" depuis le .fxml
	 */
	public AjoutPanneauController() {
		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/ajoutPanneau.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setResizable(false);
			stage.setTitle("LocaCom - ajout panneau");
			/* Evenements du claviers */
			stage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				/* si on appuie szur ENTREE */
				if (event.getCode() == KeyCode.ENTER) {
					/* et que les champs sont OK */
					if (!valider.isDisabled()) {
						ajoutPanneau();
						event.consume();
					}
				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		bnAnnuler.setOnAction(e->stage.close());



		info.setText("");
		hauteurSlider.setDisable(true);
		hauteurSlider.valueProperty().addListener((observable, oldValue, newValue) -> {

			hauteur_label.setText(Double.toString(newValue.intValue()));

		});
		longueurSlider.valueProperty().addListener((observable, oldValue, newValue) -> {

			longueur_label.setText(Double.toString(newValue.intValue()));

		});
		valider.setOnMouseClicked(e -> {
			if (!ajoutPanneau()) {
				info.setText("Erreur : les informations ne sont pas correctes");
			}else {
			
			}

			

			// Récupère les données du formulaire
		});





		visi.setItems(Visibilite.getVisibilites());
		visi.setValue(Visibilite.getVisibilites().get(3));
		/* On désative le bouton valider si les champs sont vides */
		valider.disableProperty().bind(
			    Bindings.isEmpty(prix.textProperty()).or(Bindings.isEmpty(details.textProperty()))
			);

	}


	/**
	 * Permet d'ajouter le panneau selon son format
	 * 
	 * @return TRUE si tout l'ajout du panneau s'est bien passé FALSE sinon
	 */
	private boolean ajoutPanneau() {
		boolean success = false;

		if (format_rectangle.isSelected()) {
			System.out.println("rectangle");
			success = Panneau.ajouterPanneau(prix.getText(), visi.getSelectionModel().getSelectedItem(),
					(int) longueurSlider.getValue(), (int) hauteurSlider.getValue(), details.getText());
		} else {

			success = Panneau.ajouterPanneau(prix.getText(), visi.getSelectionModel().getSelectedItem(),
					(int) longueurSlider.getValue(), (int) longueurSlider.getValue(), details.getText());
		}
		return success;

	}

	/**
	 * Méthode pour changer le format d'un panneau (rectangle / carré)
	 */
	public void swtichFormat() {

		if (!format_rectangle.isSelected()) {
			hauteurSlider.setDisable(true);
		} else {
			hauteurSlider.setDisable(false);
		}

	}


	/**
	 * @return the stage
	 */
	public static Stage getStage() {
		return stage;
	}


	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		AjoutPanneauController.stage = stage;
	}


	/**
	 * @return the addSign
	 */
	public static boolean isAddSign() {
		return addSign;
	}


	/**
	 * @param addSign the addSign to set
	 */
	public static void setAddSign(boolean addSign) {
		AjoutPanneauController.addSign = addSign;
	}


	/**
	 * @return the panneau
	 */
	public static Panneau getPanneau() {
		return panneau;
	}


	/**
	 * @param panneau the panneau to set
	 */
	public static void setPanneau(Panneau panneau) {
		AjoutPanneauController.panneau = panneau;
	}


	/**
	 * @return the format_rectangle
	 */
	public RadioButton getFormat_rectangle() {
		return format_rectangle;
	}


	/**
	 * @param format_rectangle the format_rectangle to set
	 */
	public void setFormat_rectangle(RadioButton format_rectangle) {
		this.format_rectangle = format_rectangle;
	}


	/**
	 * @return the format
	 */
	public ToggleGroup getFormat() {
		return format;
	}


	/**
	 * @param format the format to set
	 */
	public void setFormat(ToggleGroup format) {
		this.format = format;
	}


	/**
	 * @return the format_square
	 */
	public RadioButton getFormat_square() {
		return format_square;
	}


	/**
	 * @param format_square the format_square to set
	 */
	public void setFormat_square(RadioButton format_square) {
		this.format_square = format_square;
	}


	/**
	 * @return the hauteurSlider
	 */
	public Slider getHauteurSlider() {
		return hauteurSlider;
	}


	/**
	 * @param hauteurSlider the hauteurSlider to set
	 */
	public void setHauteurSlider(Slider hauteurSlider) {
		this.hauteurSlider = hauteurSlider;
	}


	/**
	 * @return the hauteur_label
	 */
	public Label getHauteur_label() {
		return hauteur_label;
	}


	/**
	 * @param hauteur_label the hauteur_label to set
	 */
	public void setHauteur_label(Label hauteur_label) {
		this.hauteur_label = hauteur_label;
	}


	/**
	 * @return the longueurSlider
	 */
	public Slider getLongueurSlider() {
		return longueurSlider;
	}


	/**
	 * @param longueurSlider the longueurSlider to set
	 */
	public void setLongueurSlider(Slider longueurSlider) {
		this.longueurSlider = longueurSlider;
	}


	/**
	 * @return the longueur_label
	 */
	public Label getLongueur_label() {
		return longueur_label;
	}


	/**
	 * @param longueur_label the longueur_label to set
	 */
	public void setLongueur_label(Label longueur_label) {
		this.longueur_label = longueur_label;
	}


	/**
	 * @return the details
	 */
	public TextField getDetails() {
		return details;
	}


	/**
	 * @param details the details to set
	 */
	public void setDetails(TextField details) {
		this.details = details;
	}


	/**
	 * @return the prix
	 */
	public TextField getPrix() {
		return prix;
	}


	/**
	 * @param prix the prix to set
	 */
	public void setPrix(TextField prix) {
		this.prix = prix;
	}


	/**
	 * @return the visi
	 */
	public ChoiceBox<Visibilite> getVisi() {
		return visi;
	}


	/**
	 * @param visi the visi to set
	 */
	public void setVisi(ChoiceBox<Visibilite> visi) {
		this.visi = visi;
	}


	/**
	 * @return the valider
	 */
	public Button getValider() {
		return valider;
	}


	/**
	 * @param valider the valider to set
	 */
	public void setValider(Button valider) {
		this.valider = valider;
	}


	/**
	 * @return the bnAnnuler
	 */
	public Button getBnAnnuler() {
		return bnAnnuler;
	}


	/**
	 * @param bnAnnuler the bnAnnuler to set
	 */
	public void setBnAnnuler(Button bnAnnuler) {
		this.bnAnnuler = bnAnnuler;
	}


	/**
	 * @return the info
	 */
	public Label getInfo() {
		return info;
	}


	/**
	 * @param info the info to set
	 */
	public void setInfo(Label info) {
		this.info = info;
	}

}
