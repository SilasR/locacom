package fr.locacom.controller;

import java.awt.Label;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import fr.locacom.main.Principal;
import fr.locacom.model.Client;
import fr.locacom.model.ListeClients;

/** Controler de la fenetre liste clients */
public class ListeClientsController implements Initializable {

	/** Stage liste client */
	private Stage stage;
	@FXML
	private TableView<Client> tableClients;
	@FXML
	private TableColumn<Client, Integer> id;
	@FXML
	private TableColumn<Client, String> nom;
	@FXML
	private TableColumn<Client, String> coords;
	@FXML
	private TableColumn<Client, Integer> nbLoc;
	@FXML
	private TableColumn<Client, Integer> telephone;
	@FXML
	private Button bnClose;
	@FXML
	private Button bnAjoutClient;
	@FXML
	private Label labelClients;
	@FXML
	private TableColumn<Client, Button> edit;
	@FXML
	private TableColumn<Client, Button> delete;
	@FXML
	private Button bnDelete;
	private static ObservableList<Client> clients = FXCollections.observableArrayList();

	/**
	 * Constructeur qui charge la scene "listeClients" depuis le .fxml
	 */
	public ListeClientsController() {
		stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/listeClients.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - liste clients");
			stage.getScene().addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				/* si on appuie szur ECHAP */
				if (event.getCode() == KeyCode.CANCEL) {

					if (tableClients.getSelectionModel().getSelectedItems().size() == 1) {
						gererDelete(tableClients.getSelectionModel().getSelectedItem());
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		refresh();

		id.setCellValueFactory(new PropertyValueFactory<>("id"));
		nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		coords.setCellValueFactory(new PropertyValueFactory<>("coord"));
		nbLoc.setCellValueFactory(new PropertyValueFactory<>("nbResa"));
		telephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
		edit.setCellValueFactory(new PropertyValueFactory<>("edit"));
		delete.setCellValueFactory(new PropertyValueFactory<>("delete"));

		tableClients.setItems(getClientsList());
		bnAjoutClient.setOnAction(event -> {
			Principal.ouvrirFenetreAjoutClient();
		});
		bnClose.setOnAction(e -> {

			Principal.fermerFenetreListeClients();
		});
		bnDelete.setDisable(true);
		tableClients.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tableClients.setOnMouseClicked(e -> {
			// Si on a selectionné plus de 1 clients
			if (tableClients.getSelectionModel().getSelectedItems().size() > 1) {
				bnDelete.setDisable(false);
			} else {
				bnDelete.setDisable(true);
			}
		});
		bnDelete.setOnAction(e -> {
			for (Client c : tableClients.getSelectionModel().getSelectedItems()) {
				gererDelete(c);
			}
			bnDelete.setDisable(true);
		});

	}

	/**
	 * Méthodes pour raffraichir la liste des clients
	 */
	public static void refresh() {

		clients.clear();

		for (Client c : ListeClients.getListeClients()) {
			
			clients.add(new Client(c.getNom(), c.getCoord(), c.getNbResa(), c.getId(),c.getTelephone()));
		}

	}

	/**
	 * @return liste des clients
	 */
	public static ObservableList<Client> getClientsList() {

		return clients;
	}

	/**
	 * Gère l'édition d'un client
	 * 
	 * @param c le client a edité
	 */
	public static void gererEdit(Client c) {

		Principal.ouvrirFenetreModifClients(c);

	}

	/**
	 * gère la suppression d'un client
	 * 
	 * @param c Le client a supprimer
	 */
	public static void gererDelete(Client c) {
		/* On alerte de la suppresion */
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation");
		alert.setHeaderText("");
		alert.setContentText("Etes vous sur de vouloir supprimer " + c.getNom());

		Optional<ButtonType> result = alert.showAndWait();
		/* On recupère le resulat */
		if (result.get() == ButtonType.OK) {
			clients.remove(c);
			ListeClients.supprimerClient(c.getId());

		} else {

		}
	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the tableClients
	 */
	public TableView<Client> getTableClients() {
		return tableClients;
	}

	/**
	 * @param tableClients the tableClients to set
	 */
	public void setTableClients(TableView<Client> tableClients) {
		this.tableClients = tableClients;
	}

	/**
	 * @return the id
	 */
	public TableColumn<Client, Integer> getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(TableColumn<Client, Integer> id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public TableColumn<Client, String> getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(TableColumn<Client, String> nom) {
		this.nom = nom;
	}

	/**
	 * @return the coords
	 */
	public TableColumn<Client, String> getCoords() {
		return coords;
	}

	/**
	 * @param coords the coords to set
	 */
	public void setCoords(TableColumn<Client, String> coords) {
		this.coords = coords;
	}

	/**
	 * @return the nbLoc
	 */
	public TableColumn<Client, Integer> getNbLoc() {
		return nbLoc;
	}

	/**
	 * @param nbLoc the nbLoc to set
	 */
	public void setNbLoc(TableColumn<Client, Integer> nbLoc) {
		this.nbLoc = nbLoc;
	}

	/**
	 * @return the bnClose
	 */
	public Button getBnClose() {
		return bnClose;
	}

	/**
	 * @param bnClose the bnClose to set
	 */
	public void setBnClose(Button bnClose) {
		this.bnClose = bnClose;
	}

	/**
	 * @return the bnAjoutClient
	 */
	public Button getBnAjoutClient() {
		return bnAjoutClient;
	}

	/**
	 * @param bnAjoutClient the bnAjoutClient to set
	 */
	public void setBnAjoutClient(Button bnAjoutClient) {
		this.bnAjoutClient = bnAjoutClient;
	}

	/**
	 * @return the labelClients
	 */
	public Label getLabelClients() {
		return labelClients;
	}

	/**
	 * @param labelClients the labelClients to set
	 */
	public void setLabelClients(Label labelClients) {
		this.labelClients = labelClients;
	}

	/**
	 * @return the edit
	 */
	public TableColumn<Client, Button> getEdit() {
		return edit;
	}

	/**
	 * @param edit the edit to set
	 */
	public void setEdit(TableColumn<Client, Button> edit) {
		this.edit = edit;
	}

	/**
	 * @return the delete
	 */
	public TableColumn<Client, Button> getDelete() {
		return delete;
	}

	/**
	 * @param delete the delete to set
	 */
	public void setDelete(TableColumn<Client, Button> delete) {
		this.delete = delete;
	}

	/**
	 * @return the bnDelete
	 */
	public Button getBnDelete() {
		return bnDelete;
	}

	/**
	 * @param bnDelete the bnDelete to set
	 */
	public void setBnDelete(Button bnDelete) {
		this.bnDelete = bnDelete;
	}

	/**
	 * @return the clients
	 */
	public static ObservableList<Client> getClients() {
		return clients;
	}

	/**
	 * @param clients the clients to set
	 */
	public static void setClients(ObservableList<Client> clients) {
		ListeClientsController.clients = clients;
	}

}
