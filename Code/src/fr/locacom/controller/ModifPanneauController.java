package fr.locacom.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import fr.locacom.main.Principal;
import fr.locacom.model.ListePanneaux;
import fr.locacom.model.Panneau;
import fr.locacom.model.Visibilite;

/**
 * Controller de la fenetre modification d'un panneau
 */
public class ModifPanneauController implements Initializable {

	/** Stage de la fenetre modification d'un panneau */
	private Stage stage;

	@FXML
	private RadioButton format_rectangle;

	@FXML
	private ToggleGroup format;

	@FXML
	private RadioButton format_square;

	@FXML
	private Slider hauteurSlider;

	@FXML
	private Label hauteur_label;

	@FXML
	private Slider longueurSlider;

	@FXML
	private Label longueur_label;

	@FXML
	private TextField details;
	@FXML
	private TextField prix;
	@FXML
	private ChoiceBox<Visibilite> visi;
	
	@FXML
	private Button bnAnnuler;
	
	@FXML
	private Button valider;
	@FXML
	private Label info;
	@FXML
	private Text lbTitre;
	/**
	 * Panneau qu'il faut modifier
	 * 
	 * @see Panneau
	 */
	private Panneau panneau;

	/**
	 * Constructeur qui charge la scene "ajouterPanner" depuis le .fxml
	 * 
	 * Avec en parametre le panneau a modifier pour remplir les champs
	 * @param p Le panneau a modifié
	 */
	public ModifPanneauController(Panneau p) {
		stage = new Stage();
		panneau = p;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fr/locacom/view/ajoutPanneau.fxml"));

			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("LocaCom - modif panneau");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode pour envoyer les modifications du panneau
	 */
	private void modifPanneau() {

		ListePanneaux.modifierPanneau(panneau.getId(), details.getText(), Double.valueOf(prix.getText()),
				(int) longueurSlider.getValue(), (int) hauteurSlider.getValue(), visi.getValue());
		Principal.fermerFenetreModifPanneau();
		ListePanneauxControler.refresh();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		hauteurSlider.valueProperty().addListener((observable, oldValue, newValue) -> {

			hauteur_label.setText(Double.toString(newValue.intValue()));

		});
		longueurSlider.valueProperty().addListener((observable, oldValue, newValue) -> {

			longueur_label.setText(Double.toString(newValue.intValue()));
		});
		bnAnnuler.setOnAction(e->stage.close());
		valider.setOnAction(e -> modifPanneau());
		details.setText(panneau.getDetails());
		prix.setText(Double.toString(panneau.getTarif()));
		longueurSlider.setValue(panneau.getLongueur());
		longueur_label.setText(Integer.toString(panneau.getLongueur()));
		hauteurSlider.setValue(panneau.getLargeur());
		hauteur_label.setText(Integer.toString(panneau.getLargeur()));

		visi.setItems(Visibilite.getVisibilites());
		// on renseigne la visibilité
		visi.setValue(Visibilite.getVisibilites().get(panneau.getVisibilite().getId() - 1));
		lbTitre.setText("MODIFIER UN PANNEAU");
		/* On désative le bouton valider si les champs sont vides */
		valider.disableProperty().bind(
			    Bindings.isEmpty(prix.textProperty()).or(Bindings.isEmpty(details.textProperty()))
			);

	}

	/**
	 * Permet de changer le format du panneau (rectangle / carré)
	 */
	public void swtichFormat() {

		if (!format_rectangle.isSelected()) {
			hauteurSlider.setDisable(true);
		} else {
			hauteurSlider.setDisable(false);
		}

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * @return the format_rectangle
	 */
	public RadioButton getFormat_rectangle() {
		return format_rectangle;
	}

	/**
	 * @param format_rectangle the format_rectangle to set
	 */
	public void setFormat_rectangle(RadioButton format_rectangle) {
		this.format_rectangle = format_rectangle;
	}

	/**
	 * @return the format
	 */
	public ToggleGroup getFormat() {
		return format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(ToggleGroup format) {
		this.format = format;
	}

	/**
	 * @return the format_square
	 */
	public RadioButton getFormat_square() {
		return format_square;
	}

	/**
	 * @param format_square the format_square to set
	 */
	public void setFormat_square(RadioButton format_square) {
		this.format_square = format_square;
	}

	/**
	 * @return the hauteurSlider
	 */
	public Slider getHauteurSlider() {
		return hauteurSlider;
	}

	/**
	 * @param hauteurSlider the hauteurSlider to set
	 */
	public void setHauteurSlider(Slider hauteurSlider) {
		this.hauteurSlider = hauteurSlider;
	}

	/**
	 * @return the hauteur_label
	 */
	public Label getHauteur_label() {
		return hauteur_label;
	}

	/**
	 * @param hauteur_label the hauteur_label to set
	 */
	public void setHauteur_label(Label hauteur_label) {
		this.hauteur_label = hauteur_label;
	}

	/**
	 * @return the longueurSlider
	 */
	public Slider getLongueurSlider() {
		return longueurSlider;
	}

	/**
	 * @param longueurSlider the longueurSlider to set
	 */
	public void setLongueurSlider(Slider longueurSlider) {
		this.longueurSlider = longueurSlider;
	}

	/**
	 * @return the longueur_label
	 */
	public Label getLongueur_label() {
		return longueur_label;
	}

	/**
	 * @param longueur_label the longueur_label to set
	 */
	public void setLongueur_label(Label longueur_label) {
		this.longueur_label = longueur_label;
	}

	/**
	 * @return the details
	 */
	public TextField getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(TextField details) {
		this.details = details;
	}

	/**
	 * @return the prix
	 */
	public TextField getPrix() {
		return prix;
	}

	/**
	 * @param prix the prix to set
	 */
	public void setPrix(TextField prix) {
		this.prix = prix;
	}

	/**
	 * @return the visi
	 */
	public ChoiceBox<Visibilite> getVisi() {
		return visi;
	}

	/**
	 * @param visi the visi to set
	 */
	public void setVisi(ChoiceBox<Visibilite> visi) {
		this.visi = visi;
	}

	/**
	 * @return the valider
	 */
	public Button getValider() {
		return valider;
	}

	/**
	 * @param valider the valider to set
	 */
	public void setValider(Button valider) {
		this.valider = valider;
	}

	/**
	 * @return the info
	 */
	public Label getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(Label info) {
		this.info = info;
	}

	/**
	 * @return the panneau
	 */
	public Panneau getPanneau() {
		return panneau;
	}

	/**
	 * @param panneau the panneau to set
	 */
	public void setPanneau(Panneau panneau) {
		this.panneau = panneau;
	}

}
